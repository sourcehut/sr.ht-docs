---
title: man.sr.ht Installation
---

This document covers the installation steps for
[man.sr.ht](https://git.sr.ht/~sircmpwn/man.sr.ht), a wiki service.

# Installation

man.sr.ht can be installed with the [standard package
installation process](/installation.md#installing-from-packages).

<div class="alert alert-warning">
  <strong>Warning:</strong> man.sr.ht requires a git.sr.ht instance to be
  available, as it is used as the backing storage for wikis.
</div>

## Daemons

- `man.sr.ht` — The web service.
