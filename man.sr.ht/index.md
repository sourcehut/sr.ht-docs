---
title: man.sr.ht docs
---

[man.sr.ht](https://man.sr.ht/) is a git-powered wiki that can host
documentation for your projects. Click "create wiki" at the bottom of any page
to get started (you will have to log in first).

## Repository structure

Each repository should be laid out with files and directories you want
to serve in your wiki. Markdown files (`*.md`) will be
compiled to HTML with the same Markdown rules that are [supported on the rest of
the site](/markdown). Each page will be made available at the URL of the path to
the file, except for `index.md` which is also shown at the top-level of each
directory. Other kinds of files (html, images, etc.) will not be served.

## Frontmatter

To optionally add some extra metadata to your pages, you may begin a file with
this pattern:

```
---
YAML goes here
---

Markdown starts here
```

The currently supported properties are:

- **title**: set the article title
- **toc**: set to false to disable the table of contents

## Publishing your changes

Publishing your changes is as easy as committing them and pushing them
upstream. You can collaborate with others on your wiki the same way you
do on your git.sr.ht projects — a mailing list on lists.sr.ht or
any other approach that you like.

## Settings

Click "manage your wiki" (you will need to be logged in and viewing the wiki you
want to manage) to access wiki settings.

### Visibility

Visibility of the wiki is managed separately to the backing git repo. This
allows you to create private repos with publicly accessible wikis. Wikis that
are public and unlisted can be viewed by any user regardless of whether they are
logged in or not. Private wikis can only be viewed by the owner.

### Delete

You can delete your wiki from this page. This operation cannot be undone. You
may optionally delete the backing repo, but be aware that this too cannot be
undone.
