# sourcehut

This is the main project hub for SourceHut itself. To browse our source code,
see the "sources" tab at the nav along the top of the page.

## Getting started with SourceHut

For tutorials and documentation, see [man.sr.ht](https://man.sr.ht). Once you've
created an account, you can move quickly between each service using the nav
across the top of the page.

## Deploying SourceHut yourself

To install SourceHut on your own hardware, see
[man.sr.ht/installation.md](https://man.sr.ht/installation.md)

## Getting help

Support is available via the [sr.ht-discuss] mailing list, on IRC at #sr.ht on
irc.libera.chat or by emailing support ([sr.ht-support]) directly &mdash; the
latter is usually best for account or billing issues.

## Contributing to SourceHut

We welcome third-party patches. If you have any changes you'd like to make to
SourceHut, please send a patch to the
[sr.ht-dev] mailing list. Feel free to
start discussions about development here as well, if your change requires some
thought before writing the code.

[sr.ht-dev]: https://lists.sr.ht/~sircmpwn/sr.ht-dev
[sr.ht-discuss]: https://lists.sr.ht/~sircmpwn/sr.ht-discuss
[sr.ht-support]: mailto:~sircmpwn/sr.ht-support@lists.sr.ht
