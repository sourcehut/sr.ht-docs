---
title: Organizing projects
---

One of the key features that differentiates SourceHut from many other forges is
the concept of projects as collections of arbitrary resources. This makes for a
powerful tool, but might take some getting used to if you are accustomed to
working with other forges. To help with that, this tutorial will cover the most
important aspects of organizing your projects.

## Understanding projects 

On other forges, a project is usually implicitly equivalent to a source code
repository. A repository can for example have one issue tracker, and maybe also
one wiki. If you create a new repository, you have essentially created a new
project.

On SourceHut, things are a little different. Projects are something you can
create and manage independently from anything else. They are managed on the
[project hub](https://sr.ht) (sometimes referred to as
[hub.sr.ht](https://man.sr.ht/hub.sr.ht/)). Most importantly, creating a new
source code repository _does not create a new project_. You will have to do so
explicitly.

Projects serve two purposes: they provide discoverability and link together
other resources. They might for example link source code repositories with bug
trackers and mailing lists. Unlike other forges, SourceHut projects do not put
any constraints on the cardinality of these links. A project can contain
multiple source code repositories, and an issue tracker can be part of several
projects.

## Features that require projects

Even if all you need is a single source code repository, the one thing that
only a project can provide is _discoverability_. All of SourceHut's
exploration and search features operate on projects: their description, their
[tags](https://man.sr.ht/hub.sr.ht/#best-practices-for-tags), etc. So if you
want others to be able to find your work, make sure to create a project for
it.

SourceHut also has a bunch of cross-service features that can only work if
certain resources are linked by means of being part of the same project. Some
common examples include:

* Referencing tickets in [commit messages](https://man.sr.ht/git.sr.ht/#referencing-tickets-in-git-commit-messages)
* Patches to mailing lists [triggering builds](https://man.sr.ht/builds.sr.ht/#hubsrht)

All these examples use resources from different services, so to determine what
is allowed and what isn't, SourceHut relies on the project owner to link the
resources through projects.

## Common project layouts

To give you some sense of how projects are commonly organized, here are a few
examples:

* [SourceHut](https://sr.ht/~sircmpwn/sourcehut/) itself is a good example of a
  reasonably complex, but otherwise not unusual project: it has multiple source
  code repositories, multiple mailing lists, and multiple issue trackers
* The [vim](https://sr.ht/~vim-project/vim/sources) folks maintain both a git
  and a mercurial repository - both part of the same project
* Turning things around, many developers employ the concept of a [public
  inbox](https://lists.sr.ht/~sircmpwn/public-inbox), a catch-all mailing list
  on which they accept patches for various smaller projects, by linking the
  list to each of them

In the end, the project layout has to match _your_ needs. But the flexibility
of SourceHut's approach to projects comes with one chore: you need to think
about it, and configure it explicitly. 
