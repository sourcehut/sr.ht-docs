---
title: Setting up your account and first git repository
---

If this is your first time using git, we recommend starting with the [Git
Book](https://git-scm.com/book/en/v2). It's free, translated into many
languages, and easy to read. Read at least the first 3 chapters.

<div class="alert alert-info">
  Prefer Mercurial? Check out the corresponding
  <a href="set-up-account-and-hg.md">Mercurial tutorial</a>.
</div>

Thanks for signing up for sr.ht! Let's start by setting up your profile details.
Your profile page is on meta.sr.ht, the sr.ht account management service. You
can fill in some basic (and optional) details like your bio on your [profile
page](https://meta.sr.ht/profile). Before we can get any work done, however, we
need to set up your SSH key and add it on the keys page.

## Generating an SSH key

sr.ht does not support pushing to git repositories over HTTPS with a
username+password — SSH keys are mandatory. If you already have an SSH key, you
can skip this step. If not, run the following command to generate one:

    ssh-keygen

If you accept the defaults, the public key will be written to
`~/.ssh/id_rsa.pub` and the private key to `~/.ssh/id_rsa`.

## Uploading your key to meta.sr.ht

The meta.sr.ht [keys page](https://meta.sr.ht/keys) has a form for adding your
SSH key. If you followed the earlier instructions to generate an SSH key, your
public key is stored at `~/.ssh/id_rsa.pub`. Copy the contents of this file to
your clipboard and paste it into the text field. Click "Add key" and your key
will now be valid for pushing to git repositories.

### Specifying a Key

If you already use multiple SSH keys, remember to specify which key to use 
with `sr.ht` services. Edit `~/.ssh/config` and add an appropriate `Host` entry
that gives the path to the private key you wish to use:

    Host *sr.ht
      IdentityFile ~/.ssh/srht.id_rsa
      PreferredAuthentications publickey

## Creating a git repository

If you already have a git repository you want to push to git.sr.ht, you can skip
this step. If not, open up a shell and run the following commands to create a
test repository for experimenting with:

    mkdir example
    cd example
    git init
    echo "Hello world!" >README.md
    git add README.md
    git commit -m "Initial commit"

This created a new git repository and added a `README.md` file to it, then
created the initial commit.

## Pushing your repository to git.sr.ht

The following commands will add a "remote" to your local git repository, which
will allow you to push changes to a remote repository on git.sr.ht.

    git remote add origin git@git.sr.ht:~username/example

Make sure to replace `username` with your own. Then this command will push your
master branch to git.sr.ht:

    git push -u origin master

Since this repository didn't previously exist, you'll be prompted with a link to
create the repository on git.sr.ht — click that link and fill out the form on
that page. You'll be redirected to your repository on git.sr.ht: you're done!

<div class="alert alert-primary">
    <strong>Tip</strong>: You can create repositories on the web on the
    git.sr.ht <a href="https://git.sr.ht/create">new repository page</a>.
</div>

---

Next: [Getting started with builds.sr.ht](/tutorials/getting-started-with-builds.md)

Other resources:

- [The git book](https://git-scm.com/book/en/v2)
- [git.sr.ht user manual](/git.sr.ht)
