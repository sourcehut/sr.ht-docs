---
title: "Setting up your account & first Mercurial repository"
---

If this is your first time using Mercurial, we recommend starting with the
[official Mercurial Guide](https://www.mercurial-scm.org/guide).

<div class="alert alert-info">
  Prefer git? Check out the corresponding
  <a href="set-up-account-and-git.md">git tutorial</a>.
</div>

Thanks for signing up for sr.ht! Let's start by setting up your profile details.
Your profile page is on meta.sr.ht, the sr.ht account management service. You
can fill in some basic (and optional) details like your bio on your [profile
page](https://meta.sr.ht/profile). Before we can get any work done, however, we
need to set up your SSH key and add it on the keys page.

## Generating an SSH key

sr.ht does not support pushing to Mercurial repositories over HTTPS with a
username+password — SSH keys are mandatory. If you already have an SSH key, you
can skip this step. If not, run the following command to generate one:

    ssh-keygen

If you accept the defaults, the public key will be written to
`~/.ssh/id_rsa.pub` and the private key to `~/.ssh/id_rsa`.

## Uploading your key to meta.sr.ht

The meta.sr.ht [keys page](https://meta.sr.ht/keys) has a form for adding your
SSH key. If you followed the earlier instructions to generate an SSH key, your
public key is stored at `~/.ssh/id_rsa.pub`. Copy the contents of this file to
your clipboard and paste it into the text field. Click "Add key" and your key
will now be valid for pushing to Mercurial repositories.

### Specifying a Key

If you already use multiple SSH keys, remember to specify which key to use 
with `sr.ht` services. Edit `~/.ssh/config` and add an appropriate `Host` entry
that gives the path to the private key you wish to use:

    Host *sr.ht
      IdentityFile ~/.ssh/srht.id_rsa
      PreferredAuthentications publickey

## Creating a Mercurial repository

If you already have a Mercurial repository you want to push to hg.sr.ht, you can
skip this step. If not, open up a shell and run the following commands to create
a test repository for experimenting with:

    mkdir example
    cd example
    hg init
    echo "Hello world!" >README.md
    hg add README.md
    hg commit -m "Initial commit"

This created a new Mercurial repository and added a `README.md` file to it, then
created the initial commit.

## Pushing your repository to hg.sr.ht

Run the following command to push your changes to hg.sr.ht:

    hg push ssh://hg@hg.sr.ht/~{{{srht_username}}}/example

Since this repository didn't previously exist, you'll be prompted with a link to
create the repository on hg.sr.ht — click that link and fill out the form on
that page. You'll be redirected to your repository on hg.sr.ht: you're done!

You can save yourself some typing and just run `hg push` next time by adding
this to `.hg/hgrc`:

    [paths]
    default = ssh://hg@hg.sr.ht/~{{{srht_username}}}/example

<div class="alert alert-primary">
    <strong>Tip</strong>: You can create repositories on the web on the
    hg.sr.ht <a href="https://hg.sr.ht/create">new repository page</a>.
</div>

---

Next: [Getting started with builds.sr.ht](/tutorials/getting-started-with-builds.md)

Other resources:

- [Mercurial: the definitive guide](https://book.mercurial-scm.org/)
- [hg.sr.ht user manual](/hg.sr.ht)
