---
title: Getting started with builds.sr.ht
---

builds.sr.ht is our build automation platform. We're going to walk through the
process of running jobs on builds.sr.ht and a look at a few useful features.

## Build manifests

Unlike platforms like Jenkins, builds.sr.ht does not allow you to pre-configure
jobs. And unlike platforms like Travis, jobs are not inherently tied to a git
repository. Each job on builds.sr.ht is described ad-hoc with a build manifest,
which can be submitted to builds.sr.ht for processing.

Let's start with a basic manifest:

```yaml
image: alpine/edge
tasks:
- example: |
    echo "hello world"
```

This is a build manifest, written in [YAML](http://yaml.org/). When we submit
this to builds.sr.ht, it will boot up an [Alpine
Linux](https://alpinelinux.org/) virtual machine using the edge release of
Alpine Linux. Then it will execute each of our build tasks — in this case,
saying "hello world".

## Submitting jobs on the web

builds.sr.ht has a web submission form, where you can paste a build manifest and
submit the job without any additional configuration. This is a useful way of
testing build manifests before giving them a permanent home, or running one-off
tasks. Visit the [job submission form](https://builds.sr.ht/submit) and paste in
the example manifest. Add a note, perhaps "my first job", and click "submit" to
run the job.

You'll be redirected to the job detail page. In a moment, one of our job runners
will pick up the task and start processing it. Within a few seconds, you should
see "hello world" shown under the "example" task.

## Adding git repositories to builds

Let's try a new build manifest. This one is going to compile and test the
[scdoc](https://git.sr.ht/~sircmpwn/scdoc) project.

```yaml
image: alpine/edge
sources:
- https://git.sr.ht/~sircmpwn/scdoc
tasks:
- build: |
    cd scdoc
    make
- test: |
    cd scdoc
    make check
```

<div class="alert alert-warning">
  <strong>Using Mercurial?</strong> Use `hg+https` and `hg.sr.ht` for the source
  URL.
</div>

Before starting your tasks, builds.sr.ht will clone each repository listed in
"sources" to the build environment. You can have as many or as few (including
zero) git repositories as you like.

## Adding dependencies

scdoc is a simple project with no dependencies. Let's try a slightly more
complex one: [mrsh](https://git.sr.ht/~emersion/mrsh), which depends on
[meson](https://mesonbuild.com/). Here's a build manifest for it:

```yaml
image: alpine/edge
packages:
- meson
sources:
- https://git.sr.ht/~emersion/mrsh
tasks:
- configure: |
    cd mrsh
    meson build
- build: |
    cd mrsh
    ninja -C build
- test: |
    cd mrsh
    ninja -C build test
```

This time, builds.sr.ht will install [Alpine Linux's meson
package](https://pkgs.alpinelinux.org/package/edge/main/x86_64/meson) before
starting your build. This uses Alpine's native `apk` package manager — other
images use different package managers.

## Testing on other platforms

Portability is important — so let's try running the same manifest on another
operating system.

```yaml
image: freebsd/latest
packages:
- meson
sources:
- https://git.sr.ht/~emersion/mrsh
tasks:
- configure: |
    cd mrsh
    meson build
- build: |
    cd mrsh
    ninja -C build
- test: |
    cd mrsh
    ninja -C build test
```

This one happens to work without any changes, but note that some images have
different names for packages, different distributions of coreutils, and so on.

## Adding these builds to your git repository

If you put a build manifest in `.build.yml` at the top of your repo, a build job
will be created each time you push new commits. You can also create multiple
manifests, for example to test multiple platforms, by putting several build
manifests at `.builds/*.yml`.

---

<div class="alert alert-info">
  <strong>Want to learn more about builds.sr.ht?</strong>
  Check out all of our <a href="/tutorials/builds.sr.ht">builds.sr.ht tutorials</a>.
</div>

Other resources:

- [builds.sr.ht user manual](/builds.sr.ht)
- [Build manifest reference](/builds.sr.ht/manifest.md)
