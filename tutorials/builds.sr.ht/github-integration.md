---
title: Integrating builds.sr.ht with GitHub
---

This is an adaptation of our [getting started with
builds.sr.ht](/tutorials/getting-started-with-builds.md) tutorial, but for code
hosted on GitHub.

## Build manifests

Unlike platforms like Jenkins, builds.sr.ht does not allow you to pre-configure
jobs. And unlike platforms like Travis, jobs are not inherently tied to a git
repository. Each job on builds.sr.ht is described ad-hoc with a build manifest,
which can be submitted to builds.sr.ht for processing.

Let's start with a basic manifest:

```yaml
image: alpine/edge
tasks:
- example: |
    echo "hello world"
```

This is a build manifest, written in [YAML](http://yaml.org/). When we submit
this to builds.sr.ht, it will boot up an [Alpine
Linux](https://alpinelinux.org/) virtual machine using the edge release of
Alpine Linux. Then it will execute each of our build tasks — in this case,
saying "hello world".

## Submitting jobs on the web

builds.sr.ht has a web submission form, where you can paste a build manifest and
submit the job without any additional configuration. This is a useful way of
testing build manifests before giving them a permanent home, or running one-off
tasks. Visit the [job submission form](https://builds.sr.ht/submit) and paste in
the example manifest. Add a note, perhaps "my first job", and click "submit" to
run the job.

You'll be redirected to the job detail page. In a moment, one of our job runners
will pick up the task and start processing it. Within a few seconds, you should
see "hello world" shown under the "example" task.

## Adding git repositories to builds

Let's try building [mrsh](https://github.com/emersion/mrsh), which depends on
[meson](https://mesonbuild.com/). Here's a build manifest for it:

```yaml
image: alpine/edge
packages:
- meson
sources:
- https://github.com/emersion/mrsh
tasks:
- configure: |
    cd mrsh
    meson build
- build: |
    cd mrsh
    ninja -C build
- test: |
    cd mrsh
    ninja -C build test
```

Before starting your tasks, builds.sr.ht will clone each repository listed in
"sources" to the build environment. You can have as many or as few (including
zero) git repositories as you like. builds.sr.ht will also install [Alpine
Linux's meson package][meson] before starting your build. This uses Alpine's
native `apk` package manager — other images use different package managers.

[meson]: https://pkgs.alpinelinux.org/package/edge/main/x86_64/meson

## Testing on other platforms

Portability is important — so let's try running the same manifest on another
operating system.

```yaml
image: freebsd/latest
packages:
- meson
sources:
- https://github.com/emersion/mrsh
tasks:
- configure: |
    cd mrsh
    meson build
- build: |
    cd mrsh
    ninja -C build
- test: |
    cd mrsh
    ninja -C build test
```

This one happens to work without any changes, but note that some images have
different names for packages, different distributions of coreutils, and so on.

## Adding these builds to your GitHub repository

Since 2022-10-01, dispatch.sr.ht is now deprecated.

See [hottub](https://sr.ht/~emersion/hottub) for third-party integration.

---

<div class="alert alert-info">
  <strong>Want to learn more about builds.sr.ht?</strong>
  Check out all of our <a href="/tutorials/builds.sr.ht">builds.sr.ht tutorials</a>.
</div>

Other resources:

- [builds.sr.ht user manual](/builds.sr.ht)
- [Build manifest reference](/builds.sr.ht/manifest.md)
