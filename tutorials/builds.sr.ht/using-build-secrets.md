---
title: Handling secrets in your build manifests
---

builds.sr.ht can be used to automate the deployment of websites, signing of
packages, and more, through the use of **build secrets**. You can upload the
secret keys necessary to run your automation on the web, then make these secrets
available to CI jobs.

## Our example build manifest

Let's say we have a git repo with static HTML files that we'd like to deploy by
sending them to our web server. A simple build manifest might look like this:

```yaml
image: alpine/edge
packages:
- rsync
sources:
- https://git.sr.ht/~you/example.org
tasks:
- upload: |
    rsync -r example.org/* example.org:/var/www/
```

This is straightforward enough — but it won't work because the build won't have
authorization to log into example.org.

## Generating the secrets & preparing our server

This step will naturally be somewhat different depending on your particular
server configuration. You should start by creating a deploy user:

    useradd -m deploy

Let's also give this user permission to update `/var/www`:

    usermod -aG www-data deploy
    chgrp www-data /var/www
    chmod g+rwx /var/www

And finally, let's log in as "deploy" and generate an SSH key:

    sudo su deploy
    ssh-keygen
    # accept the defaults
    cat .ssh/id_rsa.pub >> .ssh/authorized_keys
    cat .ssh/id_rsa

This will print out the new SSH private key. Copy this to your clipboard for the
next step.

## Adding your secret to builds.sr.ht

Go to the [builds.sr.ht secret management
dashboard](https://builds.sr.ht/secrets) and select "SSH key" for secret type,
then paste your key into the text box. Click "submit" — and your new secret
should show up on the right, along with its UUID.

This UUID is used to uniquely identify this secret in build manifests. Copy this
UUID for the next step.

## Adding secrets to your build manifest

This part is easy. We can simply add a list of secret UUIDs we want to be
available in this build.

```yaml
image: alpine/edge
secrets:
- c262b238-41de-4b43-a2f9-460424dd7896
packages:
- rsync
sources:
- https://git.sr.ht/~you/example.org
tasks:
- upload: |
    rsync -r example.org/* example.org:/var/www/
```

It's as easy as that! builds.sr.ht will install this SSH key into your build
environment when you submit this build manifest. However, it will only work for
builds submitted with your user — if someone else copies and pastes this build
manifest, the SSH key will not be added to their build VM.

## Controlling the use of secrets

You may disable access to secrets when you submit a build job by passing
`secrets: false` to the [submit mutation](https://git.sr.ht/~sircmpwn/builds.sr.ht/tree/d2f8870da41d8bf18f6bc03a0a77939d2c3987c1/item/api/graph/schema.graphqls#L443)
via GraphQL. This is automatically done by most integrations where the build
manifest could be modified by an untrusted party, such as when processing
patches sent to lists.sr.ht.

However, some degree of responsibility lies with you for keeping your secrets
secure. Avoid writing build manifests that would print your secrets to the logs,
particularly if using file secrets. If a secret is ever leaked in this manner,
you should consider that secret compromised — revoke it and generate a new one.

---

<div class="alert alert-info">
  <strong>Want to learn more about builds.sr.ht?</strong>
  Check out all of our <a href="/tutorials/builds.sr.ht">builds.sr.ht tutorials</a>.
</div>

Other resources:

- [builds.sr.ht secrets manual](/builds.sr.ht/#secrets)
