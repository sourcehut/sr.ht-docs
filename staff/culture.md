---
title: Introduction to SourceHut culture
---

Welcome to SourceHut!

SourceHut's mission statement is as follows:

> We are here to make free software better. We will be honest, transparent, and
> empathetic. We care for our users, and we will not exploit them, and we hope
> that they will reward our care and diligence with success. 

This is the philosophical ethos that underlies our business. This presents
itself in the way we act. Because we are empathetic, we value accessibility,
working to make our UI easy to use for anyone, or prioritizing performance on
low-end hardware and networks, so that access to our software does not depend on
income level. We are transparent, which motivates our public ops, financial
reports, and the fact that this page is on a public wiki. We are honest, by
telling users quickly and frankly when we make mistakes that affect them, and in
explaining our incentives and motivations so they can make informed decisions
about their relationship with us.

This extends to our internal culture as well. When we make mistakes or aren't
sure what to do, we talk to each other about it, as an extension of our
principle of honesty. We are empathetic, which is why we understand and forgive
those mistakes, and care for each other as human beings before anything else.
We have a steadfast commitment to integrity in all of our affairs that we hope
can set an example for the industry as a whole, and it is our hope that you will
keep these principles in mind in all of your work with SourceHut.

# In practical terms

Think of SourceHut's engineering culture as a dynamic, mutual collaboration
between equals, who aim to support each other in achieving our shared ambitions
in free software. We have essentially attempted to reproduce the FOSS
community's collaboration environment, and to some extent, governance model, in
the context of a business.

## What should I work on?

Most SourceHut engineers choose their own work. You may work on the projects
that you find interesting and important, at your own discretion, including
projects which are not maintained by or in the direct interests of SourceHut.
You can also choose your own tasks and priorities within those projects. The
only caveat is that it must be free and open source software.

You must do this with an attitude that honors and values the feedback and advice
of your peers, and seek to establish mutual trust. For example, junior engineers,
and senior engineers who are junior to a new project or field, will generally be
well-advised to seek the advice of the more experienced peers (be it fellow
SourceHut staff, or the maintainers of a third-party project) regarding what
tasks to work on. And likewise, those maintainers and mentors will honor and
value your growth, experience, feedback, and opinions, to create a healthy
balance of trust between participants.

## Rely on your peers

**Ask questions early**. We are here to support each other. There is no shame in
not being sure of what to do, struggling with a hard problem, or having made a
mistake. The shame is in not trusting your peers to help.

## Accepting responsibilities

In addition to proactively choosing to work on projects and tasks that you find
important, you may also accept long-term responsibilities that you find
important. A simple example of this is your long-term commitments as the
maintainer of your personal FOSS projects, which you may have already made
before even joining SourceHut. You will have similar opportunities to accept
responsibilities in the future. For example, you may become responsible for
various subsystems of sr.ht software, or in third-party projects, or have
certain responsibilities to your peers and users, such as being on-call for
infrastructure issues.

This is also done at your discretion, according to your wisdom on what
responsibilities are important and suited to your skills. This is also a means
by which you can build trust with your peers and the larger community, by being
someone they can depend on.

## Communication

We have a private channel on Libera.Chat at #sr.ht.staff, which you will be
invited to. We also have the #sr.ht and #sr.ht.watercooler channels, which are
open to the public and respectively handle forge support and SourceHut-adjacent
discussions. Aim to use the right channel: if appropriate, many matters should
be discussed in public, but we needn't bother these spaces with the day-to-day
activities internal to SourceHut.

Also remember that you represent SourceHut when you communicate with the outside
world &mdash; something you are expected to do often. Remember to be respectful,
to remember the human, and to avoid flamewars. You are building a relationship
with the community. This is not to say that you shouldn't stand by your
principles, but to be respectful of those who disagree. Give your peers
feedback, but remember to praise in public and criticise in private.

## Meetings

Everyone has a bi-weekly 1-on-1 meeting with their assigned mentor. This person
is there to help you smooth along your work, lend you their ear when you ask for
advice or are having trouble, and be your advocate to the broader organization.
The scope and goals of these meetings is a matter for you and your mentor to
agree upon, and it can evolve over time. This person is also your first stop for
any formal businessy business, for anything you would talk to a manager about.
They are not, however, a manager in the traditional sense, and don't have
special authority over you.

We also have monthly all-hands meetings where we will discuss our long-term
interests, matters relevant to the whole company, updates on interesting things
that are being worked on, and so on.

Beyond this, meetings are established on an as-needed basis. For example, we may
schedule meetings with consulting clients.

## Planning

Informal planning is done in the meetings described above, but formal planning,
such as ticket tracking, agile-style planning, and so on, is minimal at
SourceHut. We find that formal systems are often the product of non-engineers
wanting to boil their engineering teams down to numbers and apersonal measures
of progress, which is not appropriate for an organization built on mutual trust
and communication.

However, it is often *useful* to have some means of tracking the things on our
mind and communicate our intentions to others. Many of the projects we work on
have bug trackers, and mailing list archives are a good place to put proposals
and RFCs. We leverage planning tools and systems as they are helpful for us to
achieve our goals, and remove them when they are not. Work with your peers to
figure out what works for your projects.

## Time off

If you need time off, take it. It is important for you to be healthy and happy,
and that means taking time off work sometimes. There are no formal limits on
time off, and no formal process to request it. Let people know when you'll be
away so that they can work around your absence. If you have responsibilities
that you won't be tending to, see to it that they're accounted for first if
reasonably possible (emergencies can justifiably preclude this).

## How and when do I get paid?

Make sure Drew has your bank information for wire transfers or direct deposit.
We prepare invoices on or near the first of the month to send out to our
clients, and we pay the monthly base to staff on this date as well. We will also
wire you payment for any consulting invoices which were paid over the previous
month at this time.

## Expenses

If you have reasonable work expenses, for instance on work-related equipment,
books, and so on, ask Drew and he'll comp you. SourceHut will also cover your
travel and accommodations for work-related events, such as conferences, if
agreed upon in advance. Keep your receipts.
