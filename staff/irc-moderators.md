---
title: Guidelines for volunteer IRC moderators
---

Generally speaking:

- Users are expected to speak to each other with respect
- The scope of each channel's on-topic subjects is to be adhered to

## \#sr.ht

The channel is on-topic with respect to SourceHut and SourceHut-adjacent topics.

Examples of on-topic discussion include:

- Questions about SourceHut features, development, installation, etc
- Questions about things closely related to SourceHut (e.g. "The upcoming Debian
  release changes xyz, will that affect my builds?")

If someone is engaging in an off-topic discussion, you may ask them to move to
#sr.ht.watercooler.

## \#sr.ht.watercooler

This is an off-topic channel, but it is not the wild west. Its scope is limited
to the kind of discussions one might have in the break room at work. Such
discussions should generally have a link to free software or sourcehut
somewhere, though it may be more tenuous than required for topics in #sr.ht.

The watercooler is not a social channel.

Examples of on-topic discussion include:

- Software, technical standards, etc
- Free software news

Off-topic:

- What you had for breakfast
- The latest bullshit from Elon Musk
- World politics

If someone is engaging in an off-topic discussion, you may ask them to move to
##sr.ht.pub (an unofficial, community-run channel).

## Executing interventions

A simple intervention might simply be a warning, in public or in private, at
your discretion. You can also make direct interventions, such as:

* Kicking a user -- `/kick username <reason>`
* Banning a user -- `/mode +b username!*@*` 
* Muting a user -- `mode +q username!*@*`

A more sophisticated approach might be required for mutes and bans in some
cases, if you grok IRC reasonably well feel free to use whatever tools are at
your disposal.

In the event that there is an overwhelming degree of spam or other issues which
you are unable to manage effectively, you may place the channel into a muted
mode with `/mode +m`. You should message a staff member in this case.

In order to perform any of these commands, you must be an operator. You can
become an operator like so:

```
/msg ChanServ op #sr.ht

or

/msg ChanServ op #sr.ht.watercooler
```

To remove your operator status once you are done using it, use `/mode -o
username`.

SourceHut staff maintain the final word on disputes, if you're unsure then act
according to your best guess and message a staff memeber to address the problem
once they're back online.
