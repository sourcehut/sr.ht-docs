---
title: Staff Resources
---

The intended audience for this part of the wiki is SourceHut staff.

- [Culture introduction and onboarding](/staff/culture.md)
- [Support procedures](/staff/support.md)
- [Guidelines for volunteer IRC moderators](/staff/irc-moderators.md)
