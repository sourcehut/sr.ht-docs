---
title: Support procedures
---

How to handle various common support issues.

## General support procedure

Support emails come in to the [sr.ht-support] mailing list, which you should
have read/write access to. When you intend to field a support request, mention
the email in the sr.ht-staff IRC channel to avoid conflicts with other staff who
might be looking at it.

[sr.ht-support]: https://lists.sr.ht/~sircmpwn/sr.ht-support

### Identity verification

User identities need to be verified, and accounts with two factor authentication
must be verified with two factors, before any disclosure of account information
via email or any modifications to their account.

Preferred factors include:

- DKIM signatures on the email
- PGP signatures on the email
- SSH key challenges
  - To sign: `ssh-keygen -Y sign -n file -f ~/.ssh/<key> message.txt`
  - To verify: check that `ssh-keygen -l -f <key>` and
    `ssh-keygen -Y check-novalidate -n file -s message.txt.sig <message.txt`
    match
- DNS challenges on their mail server address or the domain in their profile
  (e.g. "please add this random string to a DNS TXT record to verify your
  identity")
- Web challenges on the domain in their profile (e.g. "please add this random
  string to /sourcehut.txt to verify your identity)

The last four digits of the credit card on file is sufficient to prove the
user's identity **only** for the purpose of billing-related support matters,
such as cancelling or refunding their payment or transferring billing
information to a new account.

If the user is unable to verify their identity, refuse their support inquiry.

## Account deletion

Users can perform self-service account deletion by logging into their account
and running the delete process on meta.sr.ht. This is preferred to admin
intervention since it does not require us to separately verify the user's
identity. However, admins can also manually delete accounts via the user
dashboard.

## Account renames

Clarify that it's prioritized for the beta but not available now, and suggest
that they register a new account, move their data over manually (using
import/export features et al), and ask for their billing info to be transferred
to the new account.

## Need 2FA disabled to reset account password

This is a common support request, and it is important to re-enforce when
handling this request that the user needs to use two factors to prove their
identity even when requesting to have 2FA disabled; they may use alternative
approaches like PGP, SSH, DNS, etc, as described in "Identity verification"
above but need to provide two factors of some kind nevertheless.

Without a verified identity, they have no recourse but to register for a new
account. Their inaccessible account cannot be deleted.

## Cannot pay for service

Generally this ends with offering the user one year of free service and asking
them to email us again when it runs out if their situation has not changed.

We generally trust our users, and don't ask them to substantiate financial aid
requests further than a declaration of need. For instance, there is no need to
ask for a student ID to grant service on the basis of a user's student status.

Common reasons to grant free service:

- Insufficient income (e.g. students, between jobs, etc)
- Unable to pay using their preferred payment method
- Political problems (e.g. Russian sanctions)

Common reasons to reject requests for free service:

- They want free service because their FOSS project is FOSS

If in doubt, offer them free service. The user dashboard has a place to generate
invoices, set the source to "Financial aid", the amount to 0, and the term to 1
year (the default). They can re-apply after it runs out.

## Refunds

Generally speaking if a user asks for a refund relatively close to their payment
date (say, within one quarter), give them one and cancel their paid services by
updating their account type to non-paying.

## Transfer billing information to new account

Verify both accounts, using two factors if necessary, then use the meta.sr.ht
user admin UI to transfer the billing info over.
