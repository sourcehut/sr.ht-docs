---
title: Installation
---

This document covers the installation steps common to all sr.ht services.

<div class="alert alert-success">
  If you would like to hack on or contribute to sr.ht, see the
  <a href="/hacking.md" class="alert-link">Hacking</a> page.
</div>

<div class="alert alert-warning">
  <strong>Warning:</strong> Currently, the only officially supported method for
  installing sr.ht software is through packages on x86_64 Alpine Linux hosts.
</div>

sr.ht is a distributed system in which each service (e.g., git.sr.ht,
builds.sr.ht) runs independently with its own database and resources,
communicates with other services through their respective APIs and webhooks,
and is highly tolerant to the temporary or permanent absence of its peers.
This type of design allows you to choose and deploy any subset of services
depending on your needs, making it ideal for both large-scale deployments and
small-scale installations.

However, being distributed also means that the installation and deployment
process can become a bit more involving. In particular, many sr.ht services
have their own, unique requirements that necessitate extra installation steps.
As such, be sure to consult each service's installation page for more details:

- [builds.sr.ht](/builds.sr.ht/installation.md)
- [git.sr.ht](/git.sr.ht/installation.md)
- [hg.sr.ht](/hg.sr.ht/installation.md)
- [hub.sr.ht](/hub.sr.ht/installation.md)
- [lists.sr.ht](/lists.sr.ht/installation.md)
- [man.sr.ht](/man.sr.ht/installation.md)
- [meta.sr.ht](/meta.sr.ht/installation.md)
- [pages.sr.ht](/pages.sr.ht/installation.md)
- [paste.sr.ht](/paste.sr.ht/installation.md)
- [todo.sr.ht](/todo.sr.ht/installation.md)

# sr.ht-admins

If you are a sr.ht system administrator, we encourage you to subscribe to the
[sr.ht-admins](https://lists.sr.ht/~sircmpwn/sr.ht-admins) mailing list, a
low-volume list of sysadmin-oriented announcements regarding breaking changes,
security vulnerabilities, and so on.

# Prerequisites

Most sr.ht services require the following:

- A [PostgreSQL](https://www.postgresql.org/) server — persistent storage

- A [Redict](https://redict.io) server — ephemeral storage, caching, work
  distribution

- A mail server — incoming/outgoing mail

- A cron daemon — scheduled tasks

# Installing From Packages

1. Add the appropriate [SourceHut package repository](/packages.md) to your
   package manager.

2. Install your sr.ht services!

<div class="alert alert-info">
  <strong>Note:</strong> For any distribution-related issues, please contact
  the appropriate maintainers listed on the
  <a href="/packages.md" class="alert-link">Packages</a> page.
</div>

<div class="alert alert-info">
  <strong>Note:</strong> Packages ship with the correct users and groups set
  up, as well as the correct daemon configurations for your distribution's init
  system.
</div>

# Next Steps

After installing your sr.ht services, you'll need to configure them. For more
information, see the [Configuration](/configuration.md) page.
