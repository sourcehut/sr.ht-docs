---
title: todo.sr.ht Installation
---

This document covers the installation steps for
[todo.sr.ht](https://git.sr.ht/~sircmpwn/todo.sr.ht), an issue and bug
tracker service.

# Installation

todo.sr.ht can be installed with the [standard package
installation process](/installation.md#installing-from-packages).

## Daemons

- `todo.sr.ht` — The web service.
- `todo.sr.ht-lmtp` — Incoming mail service.
- `todo.sr.ht-webhooks` — Webhook delivery service.

## Configuration

See [Configuration](configuration.md).
