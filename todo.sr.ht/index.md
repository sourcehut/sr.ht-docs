---
title: todo.sr.ht docs
---

[todo.sr.ht](https://todo.sr.ht/) is an issue tracker that can be used for
your projects.

**See also**:

- [GraphQL reference](graphql.md)
- [Installation guide](installation.md)

# Getting started

New trackers can be created on the [create page][create page] which can be
accessed from the dashboard. Here you can give it a name and a description. You
can also optionally configure your tracker by either clicking on "Create &
configure" or by going to the settings tab at the top.

[create page]: https://todo.sr.ht/tracker/create

New tickets can be created using the form on the left. Existing tickets are
shown on the right where you can optionally search using the search form at the
top.

# Search filters

todo.sr.ht supports a few basic filters when searching for tickets. These
include:

- `status:open` or `status:closed`
- `label`: an optional label that is assigned to a ticket for organizational
  purposes
- `submitter`: the user who submitted the ticket. Use `me` to find tickets
  submitted by yourself
- `assigned`: tickets that are assigned to a particular user
- `no:assignee`: tickets not assigned to anyone
- `no:label`: tickets with no labels

Use the negation prefix `!` to inverse any filter. For example:

- `!assigned:me`: tickets which are not assigned to me
- `!no:label`: tickets which have at least one label applied

## Sorting tickets

By default, tickets are sorted by most recently updated. To change the sort
order, enter one of:

- `sort:created` - sort by creation time
- `sort:updated` - sort by last updated time
- `sort:comments` - sort by the number of comments

Sort direction is descending by default and can be reversed by using `rsort`
instead of `sort`:

- `sort:updated` - sort descending
- `rsort:updated` - sort ascending

It's possible to specify multiple sorting criteria:

- `sort:comments sort:created` - sort first by number of comments, then by
  creation time

# Tickets

Each ticket has its own page and number. On the ticket page, you can comment
on the ticket (if you have commenting access). If you are the tracker owner or
the ticket submitter, you can also mark the ticket as resolved.

## Mentions

You can mention other users and tickets in the body of the ticket as well
as in the comments. This will create a link to the user/ticket mentioned
as well as notify those involved.

- To mention a user, use `~user`
- To mention a ticket, use `~user/tracker#123`
    - Tickets in a tracker owned by the same user can be mentioned with
      `tracker#123`
    - Tickets in the same tracker can be simply mentioned with `#123`

Replace `user`, `tracker`, and `123` with the actual tracker owner,
tracker name, and ticket number respectively.

# Settings

If you are the tracker owner, you can perform some customizations to your
tracker.

## Details

Here you can edit the description for your tracker. At this time, the tracker
name cannot be changed.

## Access

Here you can control access to the tracker. This allows you to set different
access settings for anonymous users, logged in users, and ticket submitters.

## Delete

You can delete your tracker here. Be careful as all tickets submitted to the
tracker are deleted as well.

# Email access

Anyone with the "submit" permission can submit new tickets by sending them to
`~username/tracker-name@todo.sr.ht`, or `u.username.tracker-name@todo.sr.ht` for
poorly behaved MTAs.

Anyone with the "comment" permission can also add comments to tickets
by writing to `~username/tracker-name/ticket-id@todo.sr.ht`,
or by replying to the notification email.

If you have the "triage" permission,
the last line of your email can be one of the following commands:

- `!resolve <resolution>`: closes the ticket with the given ticket resolution
  (one of `fixed`, `implemented`, `wont_fix`, `by_design`, `invalid`,
  `duplicate`, or `not_our_bug`)
- `!reopen` to re-open a closed ticket

All emails must be sent as `text/plain`. Please take care not to [top
post](https://git-send-email.io/top-posting.html), as the full contents of your
email will be posted as a comment.

Markdown is supported.

Anyone with the "browse" permission (including anonymous users!)
can subscribe to an entire tracker by sending an email to
`~username/tracker-name/subscribe@todo.sr.ht`
and unsubscribe by sending an email to
`~username/tracker-name/unsubscribe@todo.sr.ht`.

Similarly, sending an email to
`~username/tracker-name/ticket-id/subscribe@todo.sr.ht`
or
`~username/tracker-name/ticket-id/unsubscribe@todo.sr.ht`
will (un)subscribe the sender to the ticket.
