---
title: todo.sr.ht Configuration
---

This document covers the configuration process for todo.sr.ht.

# Mail Support

Incoming emails for todo.sr.ht should be directed to the `todo.sr.ht-lmtp`
service in a manner similar to the `lists.sr.ht-lmtp` service.

<div class="alert alert-info">
  <strong>Note:</strong> See lists.sr.ht's
  <a href="/lists.sr.ht/configuration.md">configuration page</a>
  for more information on mail server configuration.
</div>
