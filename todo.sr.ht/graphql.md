---
title: todo.sr.ht's GraphQL API
toc: false
---

todo.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [todo.sr.ht GraphQL playground](https://todo.sr.ht/graphql)
- [todo.sr.ht GraphQL scheme](https://git.sr.ht/~sircmpwn/todo.sr.ht/tree/master/item/api/graph/schema.graphqls)
