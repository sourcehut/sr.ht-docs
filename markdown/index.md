---
title: Markdown on sourcehut
---

Across the site, sr.ht uses [mistletoe][] for rendering Markdown.  It
implements [CommonMark][], which is much more well-defined than the
original Markdown specification.

Additionally, some extensions to CommonMark are used.

* Plain link URLs (of the form `<scheme>://<path>`) and plain e-mail
  addresses (of the form `<name>@<domain>`) can be used, and they will
  be turned into the appropriate links (for e-mail addresses, this
  prepends `mailto:`).

[mistletoe]: https://github.com/miyuchina/mistletoe
[Commonmark]: https://spec.commonmark.org

## Post-processing

Rendered HTML is post-processed so that it only consists the following
tags and attributes (in the format `tags...: attributes...`)

- `a`: `href`, `title`
- `abbr`: `title`
- `acronym`: `title`
- `b`
- `blockquote`
- `code`
- `dd`
- `div`
- `dl`
- `dt`
- `em`
- `h1`, `h2`, `h3`, `h4`, `h5`, `h6`: `id`
- `i`
- `img`: `alt`, `height`, `width`, `src` (only HTTP/HTTPS/local URLs)
- `input`: `type=checkbox`, `checked`, `disabled`
- `li`
- `ol`
- `p`
- `pre`
- `q`
- `span`
- `strong`
- `table`
- `tbody`
- `td`
- `th`
- `thead`
- `tr`
- `ul`

All tags can have the attributes `class`, `style`, `colspan` and
`rowspan`. The only allowed classes are `row`, `col-md-*`, and
`form-control`.

Here are valid `style` attributes:

- `margin`
- `padding`
- `text-align`
- `font-weight`
- `text-decoration`
- `padding-{left,right,bottom,top}`
- `margin-{left,right,bottom,top}`
