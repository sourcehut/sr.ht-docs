---
title: SSH access to build VMs
---

You can interact directly with your builds by connecting to the runner over SSH.
This is helpful for troubleshooting your manifests, or just getting a quick
temporary environment to test something in.

When a build fails, the VM stays alive for an extra ten minutes, and you'll be
presented with instructions on how to connect to the runner via SSH, e.g.:

```
$ ssh -t builds@azusa.runners.sr.ht connect 81809
Connected to build job #81809 (failed):
https://builds.sr.ht/jobs/~sircmpwn/81809
Your VM will be terminated 4 hours from now, or when you log out.

bash-5.0 $
```

In addition to connecting to failed builds over SSH, you can add `shell: true`
to your [manifest](/builds.sr.ht/manifest.md) to always enable SSH access. Then,
you'll be able to interact with your build as it runs.
