---
title: builds.sr.ht compatibility matrix
---

If you have any special requests, please [send an
email](mailto:~sircmpwn/sr.ht-discuss@lists.sr.ht). Only architectures supported
by each Linux distribution upstream are listed, and named after the upstream
port (e.g. Debian uses "amd64" while Alpine uses "x86_64").

**Note**: support for multi-arch builds is underway, but not yet available.

The "native" column is checked if these builds run on native hardware for that
architecture, if unchecked the builds are run on emulated hardware and may
suffer from poor performance.

**Support lifecycle**

The support lifecycle for each build image follows the upstream support cycle.
Each supported upstream release is generally offered on sr.ht, as well as the
bleeding edge or development releases if applicable. No sooner than two weeks
after a release becomes unsupported upstream, it will be unsupported on
builds.sr.ht — and anyone who's submitted recent builds using those images will
get an email warning them of the impending breakage.

It's recommended that you use aliases like "alpine/latest" or "debian/testing"
rather than using a specific release in your build manifests.

<div class="clearfix"></div>

## Alpine Linux

Maintainer: Drew DeVault <sir@cmpwn.com>

Support policy: [upstream](https://alpinelinux.org/releases/)

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Alpine Edge</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: alpine/edge</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
    <tr><td><code>arch: x86</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Alpine 3.21</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: alpine/latest</code> <strong>or</strong>
        <code>image: alpine/3.21</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: x86</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Alpine 3.20</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: alpine/old</code> <strong>or</strong>
        <code>image: alpine/3.20</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: x86</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Alpine 3.19</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: alpine/older</code> <strong>or</strong>
        <code>image: alpine/3.19</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: x86</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Alpine 3.18</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: alpine/oldest</code> <strong>or</strong>
        <code>image: alpine/3.18</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: x86</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

**packages**

The packages array is installed with `apk add`.

**repositories**

To add custom apk repositories, use `repo-url key-url key-name` (separated with
spaces), where `repo-url` is the URL of the package repository and `key-url` is
a URL from where the signing key may be downloaded, and `key-name` is the name
of the file written to `/etc/apk/keys/`. If the name of the repo is prefixed
with an @, it will use that prefix in apk.

Example:

```yaml
repositories:
  sr.ht: >
    https://mirror.sr.ht/alpine/sr.ht/
    https://mirror.sr.ht/alpine/sr.ht/alpine%40sr.ht.rsa.pub
    alpine@sr.ht.rsa.pub
```

## Arch Linux

Maintainer: none ([you?](mailto:~sircmpwn/sr.ht-discuss@lists.sr.ht))

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Arch Linux</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: archlinux</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
  </tbody>
</table>

**packages**

The package array is installed with `yay -Syu` (AUR packages are transparently
installed).

**repositories**

To add custom pacman repositories, use `url#key-id`, where `url` is the URL of
the package repository and `key-id` is the ID of the published PGP key the
packages are signed with.

## Debian

Maintainer: Taavi Väänänen <taavi@debian.org>

Support policy: stable, testing, unstable, and oldstable are supported.
See also: [upstream releases](https://www.debian.org/releases/)

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Debian Bookworm (stable)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: debian/stable</code> <strong>or</strong>
        <code>image: debian/bookworm</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: armel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipsel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Debian Trixie (testing)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: debian/testing</code> <strong>or</strong>
        <code>image: debian/trixie</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
    <tr><td><code>arch: armel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipsel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Debian Sid (unstable)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: debian/unstable</code> <strong>or</strong>
        <code>image: debian/sid</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr class="table-success">
      <td><code>arch: arm64</code> (experimental)</td>
      <td style="text-align: center;">✓</td>
      <td style="text-align: center;">✗</td>
      <td>manually</td>
    </tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
    <tr><td><code>arch: armel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipsel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Debian Bullseye (oldstable)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: debian/oldstable</code> <strong>or</strong>
        <code>image: debian/bullseye</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>monthly</td>
    </tr>
    <tr><td><code>arch: armel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipsel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

**packages**

The packages array is installed with `apt-get install`.

**repositories**

To add custom repositories, specify `url release component key-id` (separated by
spaces), where `url` is the URL of the package repository, `release` is e.g.
`bullseye` or `bookworm`, `component` is e.g. `main`, `contrib`, `non-free` or
`non-free-firmware` (since `bookworm`), and `key-id` is an optional PGP key ID
to add to the system keyring. Example:

```yaml
repositories:
  sr.ht: https://mirror.sr.ht/debian/sr.ht/ bookworm main DEADBEEFCAFEF00D
```

## Fedora Linux

Maintainer: Haowen Liu <lhw@lunacd.com>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Fedora Rawhide</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: fedora/rawhide</code> <strong>or</strong>
        <code>image: fedora/42</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Fedora 41</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: fedora/latest</code> <strong>or</strong>
        <code>image: fedora/41</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Fedora 40</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: fedora/40</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
  </tbody>
</table>

**packages**

The packages array is installed with `dnf install`.

**repositories**

Given the following list of repositories in your manifest:

```yaml
repositories:
  example: https://example.org
```

The following commands will be used to configure it:

Fedora 41 and later:

    dnf config-manager addrepo --from-repofile=https://example.org
    dnf config-manager setopt example.enabled=1

Fedora 40 and earlier:

    dnf config-manager --add-repo https://example.org
    dnf config-manager --set-enabled example

## FreeBSD

Maintainer: CismonX <admin@cismon.net>

Support policy: [Upstream production
releases](https://www.freebsd.org/releases/) are supported.

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>FreeBSD current</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: freebsd/current</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: powerpc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>FreeBSD 14.x</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: freebsd/latest</code> <strong>or</strong>
        <code>image: freebsd/14.x</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: powerpc</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: powerpc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>FreeBSD 13.x</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: freebsd/13.x</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: powerpc</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: powerpc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

**packages**

The packages array is installed with `pkg install`.

**repositories**

Custom package repositories are not supported on FreeBSD builds.

## Guix System

Maintainer: Unwox <me@unwox.com>

Guix System support for builds.sr.ht is actively being maintained 
[here](https://sr.ht/~whereiseveryone/builds.sr.ht-guix). There is a
[cookbook](https://man.sr.ht/~whereiseveryone/builds.sr.ht-guix-cookbook) to get
started.

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Guix System</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: guix</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armhf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i686</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: powerpc64le</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
  </tbody>
</table>

**packages**

The packages are installed with `guix install`.

**repositories**

Specifying additional channels via build manifest is not supported.

## NetBSD

Maintainer: Michael Forney <mforney@mforney.org>

Support policy: the most recent release and the version prior are supported. See
also: [upstream release cycle](https://wiki.netbsd.org/releng/)

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>NetBSD 10.0</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: netbsd/latest</code> <strong>or</strong>
        <code>image: netbsd/10.x</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>as required</td>
    </tr>
    <tr><td><code>arch: armv6hf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7hf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7hfeb</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipseb</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipsel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64eb</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: sparc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>


<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>NetBSD 9.3</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: netbsd/9.x</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>as required</td>
    </tr>
    <tr><td><code>arch: armv6hf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7hf</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7hfeb</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipseb</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mipsel</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64eb</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: mips64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: sparc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

**packages**

The packages array is installed with `pkgin`.

**repositories**

Custom package repositories are not supported on NetBSD builds.

## NixOS

Maintainer: Francesco Gazzetta <fgaz@fgaz.me>

Support policy: NixOS upstream does not have a clear support policy, but usually
old releases continue to get security fixes for
[a short amount of time](https://nixos.wiki/wiki/Nix_channels#The_official_channels)
after they are superseded.
We ship the latest stable version (channel `nixos-YY.MM`)
and the latest bleeding edge (channel `nixos-unstable`).
We remove old versions after they are marked as "End of life" at
[status.nixos.org](https://status.nixos.org/).

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>NixOS unstable</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: nixos/unstable</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv6</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>NixOS 24.11</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: nixos/latest</code> <strong>or</strong>
        <code>image: nixos/24.11</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv6</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>NixOS 24.05</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: nixos/24.05</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv6</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
  </tbody>
</table>

**packages**

The packages array is installed with `nix-env -iA`. Since it's possible to
specify multiple channels, you must provide the full selection path, for example
`nixos.hello`.

**repositories**

To add custom channels, use `channel-name: channel-url`. The channel name is relevant,
since `channel-name: channel-url` will execute the commands `nix-channel --add
channel-url channel-name` and `nix-channel --update channel-name`.

Example:

```yaml
repositories:
  nixpkgs: https://nixos.org/channels/nixpkgs-unstable
```

By default, no user channels are present.
However, like in a fresh NixOS installation, there is a root channel named
`nixos` set to

* `https://nixos.org/channels/nixos-XX.YY` in `nixos/latest` and `nixos/XX.YY`
* `https://nixos.org/channels/nixos-unstable` in `nixos/unstable`

which is the channel the image was built from.

**flakes**

Like in a standard Nix(OS) installation, flakes are not enabled by default.
A good way to use flakes without having to pass `--extra-experimental-features`
to every `nix` invocation is to use the
[`NIX_CONFIG`](https://nixos.org/manual/nix/stable/command-ref/env-common.html#env-NIX_CONFIG)
environment variable instead:

```yaml
environment:
  NIX_CONFIG: "experimental-features = nix-command flakes"
```

## OpenBSD

Maintainer: Jarkko Oranen <oranenj@iki.fi>

Support policy: the most recent release and the version prior are supported. See
also: [upstream release cycle](https://www.openbsd.org/faq/faq5.html)

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>OpenBSD 7.6</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: openbsd/latest</code> <strong>or</strong>
        <code>image: openbsd/7.6</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: alpha</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: hppa</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: landisk</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: loongson</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: luna88k</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: macppc</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: octeon</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: power64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: sparc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>OpenBSD 7.5</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: openbsd/old</code> <strong>or</strong>
        <code>image: openbsd/7.5</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: alpha</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: armv7</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: hppa</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: landisk</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: loongson</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: luna88k</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: macppc</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: octeon</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: power64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: sparc64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

**notes**

Binary patches are applied to the base system using `syspatch`.

**packages**

The packages array is installed with `pkg_add`.

**repositories**

Custom package repositories are not supported on OpenBSD builds.

## Rocky Linux

Maintainer: Haowen Liu <lhw@lunacd.com>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Rocky Linux 8</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: rockylinux/latest</code> <strong>or</strong>
        <code>image: rockylinux/8</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: aarch64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: x86_64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
  </tbody>
</table>

**packages**

The packages array is installed with `dnf install`.

**repositories**

Given the following list of repositories in your manifest:

```yaml
repositories:
  example: https://example.org
```

The following commands will be used to configure it:

    dnf config-manager --add-repo https://example.org
    dnf config-manager --set-enabled example

## Ubuntu

Maintainer: Haowen Liu <lhw@lunacd.com>

Support cycle: all releases under standard support.
See the [upstream support schedule](https://wiki.ubuntu.com/Releases) for more
information.

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Ubuntu Noble (24.04)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: ubuntu/lts</code> <strong>or</strong>
        <code>image: ubuntu/noble</code> <strong>or</strong>
        <code>image: ubuntu/24.04</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>weekly</td>
    </tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Ubuntu Oracular (24.10)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: ubuntu/next</code> <strong>or</strong>
        <code>image: ubuntu/oracular</code> <strong>or</strong>
        <code>image: ubuntu/24.10</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Ubuntu Jammy (22.04)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: ubuntu/oldlts</code> <strong>or</strong>
        <code>image: ubuntu/jammy</code> <strong>or</strong>
        <code>image: ubuntu/22.04</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>daily</td>
    </tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

<table class="table table-sm table-hover">
  <thead>
    <tr>
      <th>Ubuntu Focal (20.04)</th>
      <th colspan="3" style="font-weight: normal; text-align: center">
        <code>image: ubuntu/focal</code> <strong>or</strong>
        <code>image: ubuntu/20.04</code>
      </th>
    </tr>
    <tr>
      <th>arch</th>
      <th style="text-align: center">supported</th>
      <th style="text-align: center">native</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr><td><code>arch: arm64</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr class="table-primary">
      <td><code>arch: amd64</code> (default)</td>
      <td style="text-align: center; color: green">✓</td>
      <td style="text-align: center; color: green">✓</td>
      <td>monthly</td>
    </tr>
    <tr><td><code>arch: i386</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: ppc64el</code></td><td style="text-align: center"></td><td></td><td></td></tr>
    <tr><td><code>arch: s390x</code></td><td style="text-align: center"></td><td></td><td></td></tr>
  </tbody>
</table>

**packages**

The packages array is installed with `apt-get install`.

**repositories**

To add custom repositories, specify `url release component key-id` (separated by
spaces), where `url` is the URL of the package repository, `release` is e.g.
`bionic` or `cosmic`, `component` is e.g. `main`, `contrib` or `non-free`, and
`key-id` is an optional PGP key ID to add to the system keyring. Example:

```yaml
repositories:
  sr.ht: https://mirror.sr.ht/debian/sr.ht/ cosmic main DEADBEEFCAFEF00D
```
