---
title: builds.sr.ht Installation
---

This document covers the installation steps for
[builds.sr.ht](https://git.sr.ht/~sircmpwn/builds.sr.ht), a continuous
integration service.

builds.sr.ht is comprised of two components: a **master server** and **job
runners**. Typically, deployments have one master and many runners, which are
distributed across multiple servers.

<div class="alert alert-info">
  <strong>Note:</strong> For smaller deployments, job runners can be installed
  alongside the master server, but
  <a href="/builds.sr.ht/configuration.md#security-model"
  class="alert-link">not without risk</a>.
</div>

# Installation

On the master server, install the `builds.sr.ht` package.

On each server hosting a job runner, install the `builds.sr.ht-worker` and
`builds.sr.ht-images` packages.

## Daemons

- `builds.sr.ht` — The web service (master server).
- `builds.sr.ht-worker` — The job runner.

## Configuration

See [Configuration](configuration.md).

## Maintenance

To safely reboot a worker without dropping any running jobs, SIGINT the process
and it will stop accepting new work and terminate once all existing jobs
are completed.
