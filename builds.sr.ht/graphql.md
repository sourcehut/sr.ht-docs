---
title: builds.sr.ht's GraphQL API
toc: false
---

builds.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [builds.sr.ht GraphQL playground](https://builds.sr.ht/graphql)
- [builds.sr.ht GraphQL scheme](https://git.sr.ht/~sircmpwn/builds.sr.ht/tree/master/item/api/graph/schema.graphqls)
