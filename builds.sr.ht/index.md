---
title: builds.sr.ht docs
---

[sr.ht-support]: mailto:~sircmpwn/sr.ht-support@lists.sr.ht

[builds.sr.ht](https://builds.sr.ht) is a service on sr.ht that allows you to
submit "build manifests" for us to work on. We spin up a virtual machine per
your specifications and run your scripts in it. This is generally used to
compile and test patches, deploy websites, build and publish packages, and so
on.

<div class="alert alert-info">
  <strong>Heads up:</strong> Unlike most services during the alpha period,
  builds.sr.ht requires users to have a
  paid account ahead of the larger <a href="https://man.sr.ht/billing-faq.md">
  beta payment migration</a>. Refer to 
  <a href="https://man.sr.ht/ops/builds.sr.ht-migration.md">this page</a> for
  details.
</div>

**See also**:

- [Build manifest reference](manifest.md)
- [Post-build triggers reference](triggers.md)
- [GraphQL reference](graphql.md)
- [Installation guide](installation.md)
- [Build image support matrix](compatibility.md)
- [SSH access to build VMs](build-ssh.md)
- [Information for build image maintainers](image-maintenance.md)
- [Internal deployment reference](deploy.md)

# How jobs are submitted

Unlike some other build systems, builds.sr.ht does not let you configure builds
on the website itself. Instead, you write build *manifests* — YAML files that
tell builds.sr.ht what to do. You can then submit these manifests via the API
and we'll assign a runner and execute your job.

For convenience, there are ways of submitting builds automatically throughout
the sr.ht ecosystem — for example by pushing to repositories on git.sr.ht.
These integrations are [discussed below](#integrations). For details on
submitting jobs via the API, see the [GraphQL reference](graphql.md).

## Build manifests

Build manifests are YAML files that contain a description of your build
environment and steps to run in that environment. A very simple example could
be:

```yaml
image: alpine/edge
tasks:
  - say-hello: |
      echo hello
  - say-world: |
      echo world
```

When you submit this build, we'll fire up a virtual machine running an
up-to-date image of Alpine Linux. Then, we'll copy your scripts into the machine
and run them one at a time. More complex build jobs will probably use more
features of the build.yml — here's an example that deploys
[web.synapse-bt.org](https://web.synapse-bt.org):

```yaml
image: archlinux
packages:
  - nodejs
  - npm
  - rsync
sources:
  - https://git.sr.ht/~sircmpwn/receptor
environment:
  deploy: synapse@synapse-bt.org
secrets:
  - 7ebab768-e5e4-4c9d-ba57-ec41a72c5665
tasks:
  - setup: |
      cd receptor
      npm install
  - build: |
      cd receptor
      npm run build:production
  - deploy: |
      cd receptor
      sshopts="ssh -o StrictHostKeyChecking=no"
      rsync --rsh="$sshopts" -rP index.html $deploy:/var/www/web.synapse-bt.org/
      rsync --rsh="$sshopts" -rP dist $deploy:/var/www/web.synapse-bt.org/
```

A [full reference](manifest.md) for build manifests is available.

## Build images

View the full list of [supported build images](compatibility.md).

## Secrets

builds.sr.ht can keep track of secrets for you, like SSH keys or PGP keys, and
include them in builds for the purpose of deployment. You can manage your
secrets at the [secrets dashboard](https://builds.sr.ht/secrets).

## Keeping your secrets a secret

If you need to reference a secret in a command line argument or shell variable,
make sure to run `set +x` first to temporarily disable detailed command logging
in the build shell. Run `set -x` again once you're done handling secret
information to re-enable command logging. You also need to be careful that
secrets are not printed to stdout or stderr by the commands which use them
&mdash; add ` >/dev/null 2>&1` to the affected commands if you need to hide this
output.

Whenever you submit a build via the API, you can pass the `secrets` parameter (a
boolean) to explicitly disable secrets. In this case, they will be discarded and
the build run without including them (it's up to you to deal with this
gracefully in your shell scripts, by the way). It is important that you use this
parameter whenever submitting a build which runs code anyone you don't trust
could have tampered with. This includes not only the build manifests themselves,
but any code run as a side-effect, like your Makefile.

This is done for you automatically whenever you submit builds using sr.ht
features. When building patches from your mailing list, sr.ht will automatically
disable secrets.

If your secret is leaked in a public *or* unlisted build, you **must** consider
it permanently compromised, revoke it from any services it provides
authentication for, and generate new secrets from scratch. To encourage users to
rotate compromised secrets, SourceHut policy is to refuse to redact secrets
leaked in this manner. If you require some time to fully address the
consequences of a secret leak, we may redact them for up to one week &mdash;
[email support][sr.ht-support] if you require this.

## Build environment

Each task's script is given a preamble that looks like this:

```sh
#!/usr/bin/env bash
. ~/.buildenv
set -xe

# Deprecated -- use hut instead
acurl() (
	set +x
	curl --oauth2-bearer "$OAUTH2_TOKEN" "$@"
)
```

The actual shell varies depending on your build image. `~/.buildenv` contains
the environment variables you specified in your manifest, but feel free to
modify it to communicate state between build steps.

The following environment variables are defined for all builds:

- **JOB_ID**: The ID assigned to this build job
- **JOB_URL**: The URL at which the build logs and information can be viewed
  with a web browser

The following environment variables are commonly added by
[integrations](#integrations):

- **BUILD_SUBMITTER**: The name of the integration which submitted the job
- **BUILD_REASON**: The reason the integration submitted the build


# Build status badges

If you add tags to your build, or enter search terms, you can use these to
create a build status badge like this (the example being the latest status of
builds.sr.ht itself):

[![builds.sr.ht status](https://builds.sr.ht/~sircmpwn/builds.sr.ht.svg)](https://builds.sr.ht/~sircmpwn/builds.sr.ht?)

The image URL and a markdown snippet are shown in the sidebar of the [search
results page](https://builds.sr.ht/~sircmpwn?search=scheduled+image+refresh) or
[tag detail page](https://builds.sr.ht/~sircmpwn/builds.sr.ht).

# Integrations

Do you have something that integrates with builds.sr.ht? Submit a patch for this
page!

## git.sr.ht

git.sr.ht will automatically submit builds for you if you store a manifest in
the repository as `.build.yml`. Each time you push, a build with this manifest
will be submitted. We'll edit the manifest's sources array to point to the ref
you just pushed. You can also submit up to 4 builds on each push by providing
`.builds/*.yml` (if more are submitted, 4 manifests will be randomly chosen).
Use `git push -o skip-ci` if you don't want to submit builds.

**Environment**

- `BUILD_SUBMITTER`: `git.sr.ht`
- `GIT_REF`: [Git reference](https://git-scm.com/book/en/v2/Git-Internals-Git-References)
  which triggered the build (e.g. `refs/heads/master`)

## hg.sr.ht

hg.sr.ht will also automatically submit builds for you. The naming conventions
and other details are the same as the process used by git.sr.ht — described
above.

## hub.sr.ht

hub.sr.ht will automatically submit builds when a patch to a repo with
`.build.yml` is sent to a mailing list (do not forget to add the project name as
a prefix to the subject of the message, for example [PATCH project-name]).

Note that builds submitted by patchset triggers ignore changes to the build
manifest (instead they run from the latest merged state) and do not have access
to secrets. For more details, see [Keeping your secrets a secret](#keeping-your-secrets-a-secret).

**Environment**

- `BUILD_SUBMITTER`: `hub.sr.ht`
- `BUILD_REASON`: `patchset`
- `PATCHSET_ID`: ID of the patchset (e.g. `20273`)
- `PATCHSET_URL`: link to the patchset (e.g. `https://lists.sr.ht/~sircmpwn/sr.ht-dev/patches/20273`)

## hut (CLI client)

The [hut](https://sr.ht/~xenrox/hut) CLI client for SourceHut provides
convenient and easily scriptable access to the builds.sr.ht build submission
process.
