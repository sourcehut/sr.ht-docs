---
title: Build manifest reference
---

A build manifest is a YAML file that describes how to perform a build.

Here's an example:

```yaml
image: archlinux
packages:
  - meson
  - scdoc
  - wayland-protocols
  - wlroots-git
  - wayland
  - libxkbcommon
  - cairo
  - pango
  - gdk-pixbuf2
  - pixman
  - libinput
  - xorg-server-xwayland
sources:
  - https://github.com/swaywm/sway
tasks:
  - setup: |
      cd sway
      meson build
  - build: |
      cd sway
      ninja -C build
```

The minimum build manifest has an image and at least one task. The various
properties available are described here:

## image

*string*

Which OS image to build in. A list of available build images can be found on the
[compatibility page](/builds.sr.ht/compatibility.md).

## arch

*string*

Which architecture to build for. See the [compatibility
page](/builds.sr.ht/compatibility.md) for a list of available architectures.

## packages

*list* (of *string*)

A list of packages to install on the image. For image-specific details,
consult the [compatibility page](/builds.sr.ht/compatibility.md)

## repositories

*dictionary* (of *string: string*)

A list of extra repositories to enable with the image's package manager. The
specific format varies by base image, [consult the compatibility
page](/builds.sr.ht/compatibility.md) for details.

## artifacts

*list* (of *string*)

A list of files to extract from the completed build environment and make
available for downloading from the jobs page. Artifacts are only uploaded for
successful jobs and are pruned after 90 days.

Note that the file names are interpreted literally: do not use `~` or any shell
code. If a relative path is used (e.g. `example/my-artifact.tar.gz`), it will be
interpreted relative to the build user's home directory.

## shell

*boolean*

Whether to keep the build VM alive after all of the tasks have finished, even if
it doesn't fail, so you can SSH in. You can also SSH in before the tasks have
finished and tail the output of the build in your terminal. [Learn more about
SSH access to build VMs](/builds.sr.ht/build-ssh.md).

## sources

*list* (of *string*)

A list of repositories to clone into the home directory of the build user in the
build environment. Optionally, prefix the protocol with the source control
management scheme, to specify a protocol other than git.
To specify a non-default [git revision](https://git-scm.com/docs/gitrevisions), append *#commit-object* to the repository.

Examples:

- `https://git.sr.ht/~sircmpwn/scdoc`: git over https
- `https://git.sr.ht/~sircmpwn/scdoc#devel`: git over https (devel branch)
- `git@git.sr.ht:~sircmpwn/scdoc`: git over SSH
    ([requires key](/builds.sr.ht/private-repos.md))
- `hg+https://hg.sr.ht/~sircmpwn/scdoc`: Mercurial over https
- `hg+ssh://hg.sr.ht/~sircmpwn/scdoc`: Mercurial over SSH
    ([requires key](/builds.sr.ht/private-repos.md))

## tasks

*list* (of *string*)

A list of scripts to execute in the build environment. These scripts are run
with the following preamble:

```sh
#!/usr/bin/env bash
. ~/.buildenv
set -xe

# Deprecated -- use hut instead
acurl() (
	set +x
	curl --oauth2-bearer "$OAUTH2_TOKEN" "$@"
)
```

~/.buildenv contains environment variables specified by the `environment`
directive.

Task names must use only lowercase alphanumeric characters, underscores or
dashes and must be &lt;=128 characters in length. Tasks are executed in the
order specified.

Each task is run in a separate login session, so if you modify the groups of the
`build` user they will be effective starting from the subsequent task.

## triggers

*list* (of *trigger*)

A list of triggers to execute post-build, which can be used to send emails
or do other post-build tasks.

See also: [Build triggers](/builds.sr.ht/triggers.md)

## environment

*dictionary* (of *string: string* OR *string: list*)

A list of key/value pairs for options to set in the build environment via
`~/.buildenv`.

## secrets

*list* (of *string*)

List of secret UUIDs to be added to the guest during the build. See also:
[secrets](/builds.sr.ht#secrets).

## oauth

*string*

If present, and secrets are enabled for this build, an OAuth 2.0 bearer token is
generated for this build with the given string as the list of grants. The
[hut](https://sr.ht/~xenrox/hut/) tool may be used in the task scripts to
perform authenticated [GraphQL API requests](https://man.sr.ht/graphql.md).
hut needs to be included in the [`packages`](#packages) list (using the name
of the [hut package for your OS](https://repology.org/project/hut/versions)).
