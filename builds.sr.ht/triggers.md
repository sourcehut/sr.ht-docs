---
title: Build triggers reference
---

At the end of a job or a job group, you can execute triggers based on the
outcome of the job.

```
triggers:
  - action: trigger type
    condition: when to execute this trigger
    ...action-specific configuration...
```

Condition may be one of the following:

- **always**: execute the trigger after every build
- **failure**: execute the trigger after a failed build
- **success**: execute the trigger after a successful build

The following actions are available:

## email

Sends an email summarizing the build results.

*Configuration*

- **to**: The value of the "To" header in the outgoing email, e.g. your email
    address or e.g. `Jim Jimson <jim@example.org>, Bob Bobson
    <bob@example.org>`. Required.
- **cc**: The value of the "Cc" header in the outgoing email. Optional.
- **in_reply_to**: The value of the "In-Reply-To" header in the outgoing email.
    Optional.

## webhook

Submits the final job status as a POST request to a specified URL.

*Configuration*

- **url**: The URL to submit the HTTP request to

The JSON payload POSTed to the provided URL looks something like this:

```
{
  "id": integer,
  "status": string,
  "setup_log": url,
  "note": string or null,
  "runner": string or null,
  "owner": {
    "canonical_name": "~example",
    "name": "example"
  },
  "tasks": [
    {
      "name": string,
      "status": string,
      "log": url
    },
    ...
  ]
}
```

## others?

Patches welcome!
