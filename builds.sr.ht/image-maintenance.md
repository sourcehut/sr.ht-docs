---
title: builds.sr.ht image maintenance
---

Thank you for volunteering to maintain a builds.sr.ht image! Here are some tips
on how to do it well. Your responsibilities are:

1. Adding new build images as the upstream vendor provides updates
2. Deprecating old build images as they fall out of upstream support
3. Maintaining the builds.sr.ht integration and docs for this image
4. First-line support for user issues for this build image

## Releasing new versions

When a new version of your distribution is released upstream, you should prepare
a patch for builds.sr.ht which makes the new version available to SourceHut
users. **You must test new image versions**.

To test new versions, create a clone of builds.sr.ht with the patch applied on
git.sr.ht, then update your image's build.yml file to point to your git
repository, and remove the deploy task. Submit this build to builds.sr.ht to
test-build your new image version from an earlier version which is already
available, and address any problems that arise during testing.

If you use symlinks (e.g. for the "latest" version of your distribution), submit
*separate* patches to add new images and to update symlinks, so that we can
build the named image (e.g. "debian/trixie") before we update the symlinks (so
that "debian/latest" doesn't point to a yet-to-be-deployed trixie image).

Submit your patch(es) to sr.ht-dev, including at least the following:

* A patch for builds.sr.ht adding the new version, with the URL for your
  successful test build provided in the timely commentary
* A patch for builds.sr.ht updating symbolic links, if necessary for your image
* A patch for sr.ht-docs updating the compatibility matricies
* An update for contrib/crontab to schedule rebuilds of the new release
* Any special instructions for deploying the image that require manual
  intervention from the patch integrator

## Deprecating old versions

We remove distribution releases from SourceHut once they are deprecated by
upstream, following a 2-week notice period for anyone who has used the
deprecated image within the 30 days prior to its deprecation.

To remove a deprecated release for your distribution, prepare patches according
to the following criteria:

* Update builds.sr.ht to drop the image. If you use symlinks, prepare two
  patches, one updating the symlinks (first patch), and one dropping the named
  release (second patch)
* Update the compatibility matrix in sr.ht-docs
* Update contrib/crontab to unschedule rebuilds of the depreacted release
* Do not mix the changes in with patches which introduce a *new* release

We will apply patches which update any symbolic links, update the compatiblity
matrix, then email affected users and wait to apply the patch removing the named
release until the 2-week notice period expires.

## Introduction of a new distribution

To introduce a new distribution or operating system, you need to prepare a patch
for builds.sr.ht which updates the images/ directory appropriately. This should
include a "functions" file, which gives the build workers information on how to
boot your image; a "genimg" script which, when run from within your target
operating system, produces the image as a qcow2 file; and a build manifest which
will automate this process.

It is recommended that you reference existing build images to get an idea of how
the system can be applied to your target.

Submit your patch to sr.ht-dev and include the following:

* Detailed instructions for bootstrapping the image from scratch for the
  reviewers and patch integrators, such as how to set up a virtual machine
  appropriately to run the genimg script. We cannot accept binaries provided by
  the patch submitter; we will run through the entire bootstrapping procedure
  independently using upstream sources.
* An update to `contrib/crontab` which adds your image to the rebuild schedule.
* A patch for sr.ht-docs adding your image's release details, including details
  on the upstream deprecation lifecycle and other details pertinent to users of
  this distribution.

You are **strongly** advised to set up a local instance of builds.sr.ht for
testing against; we will have very little patience for reviewing untested
patches of this scale.
