---
title: Private repos on builds.sr.ht
---

builds.sr.ht can use private repos on git.sr.ht and elsewhere, but you have to
configure each job with an SSH key that has access to your account.

1. Generate an SSH key (without a passphrase)
1. Add the public key [to your account](https://meta.sr.ht/keys)
1. Add the private key as a secret using the [secrets management page](https://builds.sr.ht/secrets)
1. Copy the secret's UUID into your build manifest's secrets list.
1. Update your sources list to use the SSH clone URL — not the https clone URL.

The resulting build manifest should look something like this:

```yaml
image: # ...
secrets:
- c043e12e-a297-4ece-b09c-bb5a99dc4094
sources:
- git@git.sr.ht:~yourname/your-private-repo
```
