---
title: Hacking on SourceHut
toc: false
---

This document provides information for those interested in hacking on or
contributing to SourceHut.

# Installation

Here are some tips to expedite the installation procedure:

* A PostgreSQL database and Redis server are always required.
* Some services require (or may optionally make use of) an S3-compatible
  storage backend. Minio is recommended for this purpose.
* Generally, you can skip the webhooks daemons unless you need them.
* Always prefer [package installations][1] for obtaining dependencies.
* You can *usually* get away without configuring the outgoing mail settings,
  but you may still have to generate a PGP key to get things started up.
* You can skip setting up a reverse proxy and use our development servers via
  `python3 run.py` instead.
* Some services depend on other services. Some such integrations are optional,
  but all services depend on meta.sr.ht.

[1]: /installation.md#installing-from-packages "SourceHut docs: Installing from packages"

## Dev dependencies

In addition to the runtime dependencies of our packages, building
packages from source requires additional build-time dependencies:

* `sassc` for CSS pre-processing
* `minify` for minifying CSS
* `go` for compiling the Go APIs
* `make` for making

## Generic instructions

### Install shared assets

All services rely on shared assets that are kept in
[core.sr.ht](https://git.sr.ht/~sircmpwn/core.sr.ht). They need to be installed
once (and potentially updated when they change):

* Check out [core.sr.ht](https://git.sr.ht/~sircmpwn/core.sr.ht). Make sure to
  clone with `--recurse-submodules`.
* Run `make PREFIX=~/.local install` to install the shared assets.

### Configuration

Do this once, when starting with meta.sr.ht:

* Check out [meta.sr.ht](https://git.sr.ht/~sircmpwn/meta.sr.ht).
* Copy meta.sr.ht's `config.example.ini` to `config.ini` and configure
  everything following the instructions in the comments.

When filling in the `config.ini` file, remember the following:

* Some of the comments will ask you to run programs such as `srht-keygen` —
  these can be found in the core.sr.ht repository.
* If you do not have SSL enabled for Postgres, be sure to add `?sslmode=disable`
  to the end of your Postgres URL.

### Services

Then, for each `SERVICE` (e.g. `meta.sr.ht`):

* `make PREFIX=~/.local all` (`PREFIX` must match the one used during shared
  assets installation)
* `export PYTHONPATH=/path/to/core.sr.ht:/path/to/$SERVICE`

To avoid conflicts with the `email` package, you should be careful what you run
while `$PYTHONPATH` is set like this.

Note that core.sr.ht is not a service, but merely a repository of code shared
between services.

For each service *except* meta.sr.ht:

* Copy the service's section(s) from its `config.example.ini` into meta.sr.ht's
  `config.ini`, modifying it according to the comments.
* `ln -s /path/to/meta.sr.ht/config.ini config.ini`

For each `SERVICE`:

* `createdb $SERVICE` (e.g. `createdb meta.sr.ht`)
* `psql -d $SERVICE -f schema.sql` (e.g. `psql -d meta.sr.ht -f schema.sql`)

To run a service:

* `./$SERVICE-api` (e.g. `./meta.sr.ht-api`, must be run first)
* `python3 run.py`

# Sending Patches

We use [git send-email](https://git-send-email.io) and discuss patches (and
other development topics) on the
[sr.ht-dev](https://lists.sr.ht/~sircmpwn/sr.ht-dev) list. Please send your
patches and development questions there.
