---
title: meta.sr.ht's GraphQL API
toc: false
---

meta.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [meta.sr.ht GraphQL playground](https://meta.sr.ht/graphql)
- [meta.sr.ht GraphQL scheme](https://git.sr.ht/~sircmpwn/meta.sr.ht/tree/master/item/api/graph/schema.graphqls)
