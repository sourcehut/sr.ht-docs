---
title: meta.sr.ht Configuration
---

This document covers the configuration process for meta.sr.ht.

# Cronjobs

- `metasrht-daily`: It is recommend to run this job daily.

# Billing

To enable billing, consult the `meta.sr.ht::billing` configuration section. You
will need a working Stripe account for payment processing, and may need to
install the additional `meta.sr.ht-billing` package to obtain additional
dependencies.

# Users

To create users, use `metasrht-manageuser`. For example, to create an
administrator:

    metasrht-manageuser -t admin -e <email> <username>
