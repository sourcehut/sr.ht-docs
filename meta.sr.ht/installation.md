---
title: meta.sr.ht Installation
---

This document covers the installation steps for
[meta.sr.ht](https://git.sr.ht/~sircmpwn/meta.sr.ht), the central
authentication and account service for sr.ht.

<div class="alert alert-warning">
  <strong>Warning:</strong> A working meta.sr.ht installation is a basic
  requirement of running any other sr.ht service.
</div>

# Installation

meta.sr.ht can be installed with the [standard package
installation process](/installation.md#installing-from-packages).

## Daemons

- `meta.sr.ht` — The web service.
- `meta.sr.ht-api` — The API service.
- `meta.sr.ht-webhooks` — Webhook delivery service.

## Cronjobs

- `metasrht-daily`: Purges old audit log entries and runs billing.

## Configuration

See [Configuration](configuration.md).
