---
title: meta.sr.ht docs
---

[meta.sr.ht](https://meta.sr.ht) is a service on sr.ht that manages your user
account.

**See also**:

- [GraphQL reference](graphql.md)
- [Installation guide](installation.md)

# Profile

On your [user profile](https://meta.sr.ht/profile), you can set some
aesthetic information, which will be shown publically. The bio supports
[Markdown](/markdown).

# Security

On your [security page](https://meta.sr.ht/security) you can manage two-factor
authentication and view the audit log.

## Two-factor authentication

Presently, only
[TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_Algorithm)
is supported for two-factor authentication. Any TOTP software should work
(e. g. [Aegis Authenticator](https://f-droid.org/packages/com.beemdevelopment.aegis/),
[FreeOTP](https://f-droid.org/en/packages/org.fedorahosted.freeotp/)
or Google Authenticator) because sr.ht currently uses the first iteration of
the TOTP parameters.

To enable TOTP, click "Enable TOTP" and scan the QR code with your TOTP app.
Enter a valid code to enable TOTP. Note that once enabled, you will not be able
to [reset your account password](#password-reset) without contacting support.

## Audit Log

The audit log shows a log of activity on your account and the IP address
associated with it. These logs are purged after 14 days.

# Keys

On your [keys page](https://meta.sr.ht/keys), you can manage public keys
associated with your account, which may be utilized by various services
throughout the site (such as your SSH keys being used to authorize pushes to
git.sr.ht).

# Privacy

On the [privacy page](https://meta.sr.ht/privacy), you may choose to have emails
from the sr.ht network encrypted with your PGP public key. Add your key on the
[keys page](https://meta.sr.ht/keys), then on the privacy page you can select
the key you wish to have emails encrypted with. You may also send a test email
to confirm that it worked correctly.

All emails from sr.ht will include a PGP signature using the key provided on the
privacy page, regardless of your encryption preferences. You may use this key to
verify the authenticity of our emails if you wish.

# OAuth

On the [OAuth page](https://meta.sr.ht/oauth), you can manage access to your
account that you have authorized to third parties.

## Authorized Clients

This is a list of OAuth keys issued to third parties and the resources they are
permitted to access on your account. You may revoke the third party's access to
your account with the "Revoke" button.

## Registered Clients

Registering an OAuth client allows you to build applications that can
authenticate sr.ht users and access resources on their account. On this page you
may register new clients and manage existing ones. For details on the use of
OAuth on SourceHut, visit the [OAuth documentation](oauth.md).

On the "manage" pages, you will find:

### Settings

You may edit your public client name here, as well as editing the URI sr.ht will
redirect to upon successful authorization by a user.

### Security

Should security issues come up with your OAuth application, you will find some
mitigations here. "Reset client secret" will issue you a new client secret, and
"Revoke all tokens" will revoke all tokens previously issued to your OAuth
client, requiring you to re-authorize users.

### Scopes

If you wish to integrate more deeply with sr.ht, you can have meta.sr.ht
authorize access to *your* API. On the scopes page you may create new OAuth
scopes that third parties can request permission for use on your API.

### Delete

Here you may permenantely delete your OAuth client.

## Personal Access Tokens

If you want to integrate with sr.ht APIs for your personal scripts or tools, you
can request a personal access token here. All personal access tokens expire in 1
year and have complete access to your sr.ht account across the sr.ht network and
any third parties that delegate to sr.ht for authentication to their APIs.

# Admin

**Note**: This tab is not visible to the general public, and this information is
only applicable to users running their own meta.sr.ht instances.

## Invite Links

If you run a closed instance (i.e. `[meta.sr.ht]registration=no` in your
config), this page can be used to generate one-time registration links to invite
users to create an account.

# Password Reset

You can reset your meta.sr.ht password on [this
page](https://meta.sr.ht/forgot).
