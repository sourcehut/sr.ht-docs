---
title: OAuth 2.0 via meta.sr.ht
---

meta.sr.ht offers an OAuth 2.0-compatible ([RFC 6749][RFC 6749]) authorization
process for access to the sr.ht GraphQL APIs. This document explains the
sr.ht-specific details of our RFC 6749 implementation and is intended to be
accompanied by a reading of the RFC.

[RFC 6749]: https://tools.ietf.org/html/rfc6749
[oauth dashboard]: https://meta.sr.ht/oauth2

## Constraints

Our OAuth 2.0 implementation has the following caveats:

- Only confidential clients are supported; public clients are not allowed

In the use-case of a native application, where the public client type specified
in RFC 6749 is preferred, the application must either provide a web server
component which completes the confidential authentication process and shares the
access token with the application; or the application should ask the user to
provide a personal access token, which can be generated at
[meta.sr.ht/oauth2][oauth dashboard]. In the latter case, the application should
inform the user of the grant string which supports only the minimum access level
required to use the application's features.

- The implicit grant (section 4.2) is not supported for the same reasons
- The resource owner password credentials grant (section 4.3) is not supported
- The client credentials grant (section 4.4) is not used

## Access scopes

OAuth 2.0 clients using sr.ht must specify explicitly the list of features they
wish to be granted permission to use.

The format of the scope string, per [section 3.3][RFC 6749:3.3] is a
space-delineated list of grants. Each grant has three components: service,
scope, and access kind; and is formatted as `service/scope:access kind`.

[RFC 6749:3.3]: https://tools.ietf.org/html/rfc6749#section-3.3

The service is the name of the service the access grant applies to, such as
"meta.sr.ht".

The scope is the specific feature of that service which the grant applies to,
such as "SSH_KEYS". The list of access scopes available for each API is
documented in the GraphQL schema for that API in the `enum AccessScope` type.

The access kind is either `RO` or `RW`; respectively referring to read-only or
read/write access to that scope.

Example: `meta.sr.ht/PROFILE:RO meta.sr.ht/SSH_KEYS:RW git.sr.ht/REPOS:RO`

The scopes necessary to use each GraphQL resolver are also indicated in the
GraphQL schema with the `@access` directive.

## Client registration

OAuth 2.0 clients may be registered at [meta.sr.ht/oauth2][oauth dashboard]. You
will be issued a client ID and secret per [sections 2.2 & 2.3][RFC 6749:2.2].
The client ID is a [UUID][RFC 4122], and the secret is 64-byte random string,
[base64][RFC 4648] encoded. It is not necessary to interpret them; they are
passed verbatim to our OAuth 2.0 endpoints.

[RFC 6749:2.2]: https://tools.ietf.org/html/rfc6749#section-2.2
[RFC 4122]: https://tools.ietf.org/html/rfc4122
[RFC 4648]: https://tools.ietf.org/html/rfc4648

## Authorization endpoint

The authorization endpoint (see [section 4.1.1][RFC 6749:4.1.1]) is
`https://meta.sr.ht/oauth2/authorize`. Note that meta.sr.ht differs from the
specification in that it REQUIRES the scope parameter to be provided; per
[section 3.3][RFC 6749:3.3] meta.sr.ht interprets the absence of the scope
parameter as an invalid scope and will cause the request to fail.

[RFC 6749:4.1.1]: https://tools.ietf.org/html/rfc6749#section-4.1.1

The redirect_uri parameter is prohibited by meta.sr.ht, and MUST NOT be included
in the authorization endpoint URL. The only supported redirect URI is the one
provided during client registration. If request-specific state is required,
utilize the state parameter.

The authorization code issued is a 16 character hexadecimal string, and it must
be used within 5 minutes.

## Access token endpoint

The access token endpoint (see [section 4.1.3][RFC 6749:4.1.3]) is
`https://meta.sr.ht/oauth2/access-token`. The `request_uri` parameter MUST NOT
be provided by the client. HTTP Basic authentication is also recommended per
[section 2.3.1][RFC 6749:2.3.1]. Our access token response will always set the
token type to "bearer".

[RFC 6749:4.1.3]: https://tools.ietf.org/html/rfc6749#section-4.1.3
[RFC 6749:2.3.1]: https://tools.ietf.org/html/rfc6749#section-2.3.1
