---
title: Mailing list etiquette
---

Some email clients have popularized email usage patterns which are considered
poor form on many mailing lists, including sr.ht. Please review some of our
suggestions for participating more smoothly in discussions on the platform.
This advice will likely serve you well outside of sr.ht as well. Thank you for
taking the time to adjust your habits!

# Plain text

Please make sure that your email client is configured to use plain text emails.
By default, many email clients compose emails with HTML, so you can use rich
text formatting. Rich text is not desirable for development-oriented email
conversations, so you should disable this feature and send your email as "plain
text". Every email client is different, you should research the options for your
specific client. HTML emails are rejected by all sr.ht services.

To learn about recommended clients for plaintext users and how to setup
plaintext with your email client check out our guide at
[useplaintext.email](https://useplaintext.email/).

# Top-posting

Some email clients will paste the entire email you're replying to into your
response and encourage you to write your message over it. This behavior is
called "top posting" and is discouraged on sr.ht. Instead, cut out any parts of
the reply that you're not directly responding to and write your comments inline.
Feel free to edit the original message as much as you like. For example, if I
emailed you:

    Hey Casey,

    Can you look into the bug which is causing 2.34 clients to disconnect
    immediately? I think this is related to the timeouts change last week.

    Also, your fix to the queueing bug is confirmed for the next release,
    thanks!

You might respond with:

    Hey Drew, I can look into that for sure.

    > I think this is related to the timeouts change last week.

    I'm not so sure. I think reducing the timeouts would *improve* this issue,
    if anything.

    > Also, your fix to the queueing bug is confirmed for the next release,
    > thanks!

    Sweet! Happy to help.

- A: Because it reverses the logical flow of conversation.
- Q: Why is top posting frowned upon?

# Wrap lines

Please wrap lines in your email at 72 columns. Many people use email readers
designed to faithfully display plaintext and won't break lines at a width which
is comfortable for reading, or won't break lines at all, which is useful when
reviewing patches.  Some readers also have many things open in addition to their
mail client, and may not allocate as much screen real-estate to email as you do.

Don't worry about re-wrapping lines written by anyone you're quoting unless you
want to.

# PGP Signatures

If you use PGP, please attach your signature to the message instead of using an
inline signature. Look in your local PGP implementation's documentation for
`PGP/MIME` options.

# Patches

To learn about using email for sending patches, check out our tutorial at
[git-send-email.io](https://git-send-email.io).
