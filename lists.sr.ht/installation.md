---
title: lists.sr.ht Installation
---

This document covers the installation steps for
[lists.sr.ht](https://git.sr.ht/~sircmpwn/lists.sr.ht), a mailing list service.

# Installation

lists.sr.ht can be installed with the [standard package
installation process](/installation.md#installing-from-packages).

## Daemons

- `lists.sr.ht` — The web service.
- `lists.sr.ht-lmtp` — Incoming mail service.
- `lists.sr.ht-process` — Mail processing service.
- `lists.sr.ht-webhooks` — Webhook delivery service.

## Configuration

See [Configuration](configuration.md).
