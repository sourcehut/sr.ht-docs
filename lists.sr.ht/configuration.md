---
title: lists.sr.ht Configuration
---

This document covers the configuration process for lists.sr.ht.

# LMTP daemon installation

The LMTP daemon is available in the `lists.sr.ht` package, at the binary
`lists-srht-lmtp`. It needs to run on your mail server, and you need to
configure your MTA to forward emails to it. Alternatively, it may be configured
to accept SMTP and run on another server. See `config.ini` for details.

The LMTP daemon uses the same config file as the others, and there are some
options there specifically catered to it. The most important is the Unix socket
path for the LMTP socket — and the user/group it should be assigned to. Make
sure that this is readable and writable by your MTA.

Enable the `lists.sr.ht-lmtp` service and configure your MTA to forward emails
to the socket.

# Mail Server

The precise configuration of your mail server is a very subjective matter, and
we don't have a one-size-fits-all solution to give you. Here are examples of
forwarding to lists.sr.ht with:

### Postfix

    # main.cf
    local_transport = local:$myhostname
    transport_maps = hash:/etc/postfix/transport

    # /etc/postfix/transport
    lists.sr.ht lmtp:unix:/tmp/lists.sr.ht-lmtp.sock
    sr.ht local:sr.ht

### OpenSMTPD

    # /etc/smtpd/smtpd.conf

    ...
    action listssrht lmtp "/tmp/lists.sr.ht-lmtp.sock" rcpt-to virtual { "@" = lists }
    action "relay" relay

    match from any for domain "lists.mydomain.tld" action listssrht
    match from any auth for any action "relay"
    match from local for any action "relay"

This forwards mail to @lists.sr.ht to the LMTP socket, and processes mail to
@sr.ht to local mailboxes. This is just one part of your setup, however, and
the remainder of your postfix (or other mail server) configuration is left as
an exercise to the reader.

<div class="alert alert-info">
  <strong>Note:</strong> A minimal, local mail server setup which can be used
  to work on lists.sr.ht is described in this
  <a href="https://drewdevault.com/2018/08/05/Local-mail-server.html"
  class="alert-link">article</a>.
</div>

# Celery Worker

On servers which should process forwarding and archival, install the
`lists.sr.ht` package and enable the `lists.sr.ht-process` service. Make sure
you review the config file for worker-specific options as well. The Redis
instance you use here should be shared with the LMTP daemon and other worker
nodes to ensure that messages can be distributed correctly.
