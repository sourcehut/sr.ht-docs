---
title: lists.sr.ht's GraphQL API
toc: false
---

lists.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [lists.sr.ht GraphQL playground](https://lists.sr.ht/graphql)
- [lists.sr.ht GraphQL schema](https://git.sr.ht/~sircmpwn/lists.sr.ht/tree/master/item/api/graph/schema.graphqls)
