---
title: lists.sr.ht docs
---

[lists.sr.ht](https://lists.sr.ht) is the sr.ht mailing list service.

**See also**:

- [GraphQL reference](graphql.md)
- [Installation guide](installation.md)

# New to mailing lists?

We have some resources for you:

- [Mailing list etiquette](etiquette.md)
- [Code review on lists.sr.ht](code-review.md)

If you plan on contributing patches, we also have these guides:

- [Using git-send-email for sending and reviewing patches on sr.ht](/git.sr.ht/send-email.md)
- [Using hg-email for sending and retrieving patches on sr.ht](/hg.sr.ht/email.md)

# lists.sr.ht manual

The following sections document various features of lists.sr.ht.

# Posting

You may post to a list that you have posting permissions on by writing a
plaintext email to `~user/list-name@lists.sr.ht`. If your MTA doesn't support
characters like ~ and / in emails, please write to postmaster@your-mta.com
asking them to fix the bug, then use `u.username.list-name@lists.sr.ht` instead.

# Email controls

You may subscribe to any list by emailing
`~user/list-name+subscribe@lists.sr.ht`. You may unsubscribe with
`+unsubscribe`. You may post new threads to this list by writing to the address
with no `+` command.

Email replies to patchset threads can include a custom header,
`X-Sourcehut-Patchset-Update` to update the status of the patchset to
`PROPOSED`, `NEEDS_REVISION`, `SUPERSEDED`, `APPROVED`, `REJECTED`, or
`APPLIED`.

# Dashboard

Your [dashboard](https://lists.sr.ht) shows you recent emails on mailing lists
you're subscribed to. You can reply to one by clicking the author's name, or
view the thread by clicking the subject.

# Profile

Your public profile page shows a feed of emails authored by you, as well as a
list of mailing lists you administrate. Like the dashboard, you can
reply to emails by clicking the authors name and view the thread by clicking
the subject.

# Archive

Each list shows a list of archives, sorted by which has seen the most recent
activity. In each thread's heading, you can see the number of participants,
number of replies, and subject of the initial message. Click the subject to see
the full thread.

## Search filters

The search bar allows you to search list archives. Optional filters include:

- `from:` the author of the message. Use `me` to search for messages sent by
yourself
- `is:patch` only show messages that contain a patch
- `In-Reply-To:` show messages that are replies to a given message ID
- `message-id:` find message with a given message ID
- Other arbitrary mail headers

Surround search terms with double-quotes to return exact matches.

# Threads

Email threads can become trees as participants reply to different messages. In
the simple case of a linear thread, you will see replies written linearly.
However, if a thread becomes split, you may see several linear trees of
discussion form.

To reply to a message, click the author's email address.

## Downloading messages

Additionally, you can download raw messages from each thread, either
individually or as a whole in an mbox format. This is useful for applying
submitted patches via `git am`/`hg import` among other uses.

Access raw individual messages by clicking on "Details" and following
the link there.

Access Mbox files by clicking on "Export thread (mbox)" in the
sidebar.

# List Administration

List access controls are available in your list settings, accessed
with the "List settings" button on the archive page. The controls are
fine-grained enough to support many access scenarios, here are some examples:

## Announcement lists

A list that only you can write to is useful for announcements. To create such
a list, remove all users' "post" and "reply" permissions, to prevent them from
submitting. Owners are always able to post, of course. You could optionally
leave the "reply" permission enabled to allow people to respond to announcements,
but beware that their responses will be sent out to all subscribers, which is
usually undesirable for low-volume announcement lists.

## Write-only security lists

If you want a mailing list where people can write to you about security
vulnerabilities in your software, you can remove the "browse" permission without
removing the "post" or "reply" permissions from the list. This will allow people
to send emails to the list, but not view the archives or subscribe.
