---
title: api.sr.ht docs
---

api.sr.ht is the SourceHut GraphQL API gateway. It is responsible for merging
the individual GraphQL APIs of each service into a single federated graph
through GraphQL federation.

<div class="alert alert-info">
  <strong>Note:</strong> GraphQL federation is still in the process of being
  rolled out across sr.ht services. For more information, see
  <a href="/ops/federation.md" class="alert-link">the roll-out plan</a>.
</div>

**See also**:

- [Installation guide](installation.md)
