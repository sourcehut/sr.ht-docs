---
title: api.sr.ht Installation
---

This document covers the installation steps for
[api.sr.ht](https://git.sr.ht/~sircmpwn/api.sr.ht), the GraphQL API gateway.

# Installation

Install the `api.sr.ht` package.

## Daemons

- `api.sr.ht` — The GraphQL API gateway

## Maintenance

The gateway does not maintain any state except for the federated graph it
constructs by merging the GraphQL APIs of each service.

If polling is configured, the gateway will periodically poll the GraphQL API of
each service to check for changes to the schema and rebuild the merged schema if
necessary. Otherwise, the gateway will need to be manually restarted to rebuild
the merged schema.
