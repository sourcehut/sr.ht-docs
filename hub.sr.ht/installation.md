---
title: Project hub installation
---

[hub.sr.ht](https://git.sr.ht/~sircmpwn/hub.sr.ht) (or the "project hub")
indexes and organizes projects for sr.ht communities.

# Installation

hub.sr.ht is a standard sr.ht web service and can be installed through the
[standard procedure](/installation.md). However, its utility depends on the
installation and configuration of at least one or two other web services. See
the top-level [installation guide](/installation.md) for a list of these
services.

## Daemons

- `hub.sr.ht`: the web service

## Cronjobs

(none)
