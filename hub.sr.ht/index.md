---
title: Project hub docs
---

[hub.sr.ht](https://sr.ht) (or the "project hub") is a service which indexes and
organizes projects on sr.ht, and the resources those projects use on other
services.

**See also**:

- [Installation guide](installation.md)

# Best practices for #tags

Tags aid in community building and project discovery. If we are consistent about
how we use them, it will help meet those goals better. Here are some
recommendations for your project tags:

- Avoid adding tags for implementation language unless it matters (e.g.
  language-specific libraries)
- Avoid adding tags for platform support unless your project is based on
  platform-specific features (e.g. seccomp-related projects should use #linux)
- If there are multiple ways to word a tag (e.g. #gfx vs #graphics), check if
  there's already an existing preference you can join
