---
title: Getting support
---

Here's where to find support resources for sr.ht.

# Reaching the support team

You can reach the support team by emailing
[~sircmpwn/sr.ht-support](mailto:~sircmpwn/sr.ht-support@lists.sr.ht). This is
your best bet for account-related issues, billing problems, and so on.

Support is provided at the best effort of the sysadmins, who have other things
to do as well. It's common to have to wait for a few days to receive an answer
to your question. Please be patient.

# Asking the community

A great place to ask for general questions with using sr.ht, starting
discussions about your ideas and feature requests, and generally getting to know
the sr.ht community is the
[sr.ht-discuss](https://lists.sr.ht/~sircmpwn/sr.ht-discuss) mailing list. You
can post here by writing an email to
[~sircmpwn/sr.ht-discuss@lists.sr.ht](mailto:~sircmpwn/sr.ht-discuss@lists.sr.ht).
Please review the [mailing list etiquette guide](/lists.sr.ht/etiquette.md)
first! Be sure to search through the archives, as your question may have been
asked before.

# Real time chat

Both community members and admins hang out at [#sr.ht on irc.libera.chat][0],
where you're welcome to ask questions and have discussions in a more free-form,
real-time context. There's not always someone paying attention here, so be
prepared to wait or follow-up with an email if necessary. Please review the
[irc etiquette guide](/chat.sr.ht/etiquette.md) first!

[0]: irc://irc.libera.chat/#sr.ht

# Submitting bug reports

<div class="alert alert-warning">
  <strong>Note:</strong> the sr.ht bug trackers are meant for <i>confirmed bugs
  and feature requests</i> only, not for end-user support. Please discuss 
  issues or features on the mailing list or on IRC — you will be asked to file
  a ticket if appropriate.
</div>

Each service of sr.ht has a dedicated bug tracker:

- [hub.sr.ht](https://todo.sr.ht/~sircmpwn/hub.sr.ht)
- [git.sr.ht](https://todo.sr.ht/~sircmpwn/git.sr.ht)
- [hg.sr.ht](https://todo.sr.ht/~sircmpwn/hg.sr.ht)
- [builds.sr.ht](https://todo.sr.ht/~sircmpwn/builds.sr.ht)
- [todo.sr.ht](https://todo.sr.ht/~sircmpwn/todo.sr.ht)
- [lists.sr.ht](https://todo.sr.ht/~sircmpwn/lists.sr.ht)
- [man.sr.ht](https://todo.sr.ht/~sircmpwn/man.sr.ht)
- [meta.sr.ht](https://todo.sr.ht/~sircmpwn/meta.sr.ht)
- [paste.sr.ht](https://todo.sr.ht/~sircmpwn/paste.sr.ht)
- [pages.sr.ht](https://todo.sr.ht/~sircmpwn/pages.sr.ht)

There is also a [general-purpose bug
tracker](https://todo.sr.ht/~sircmpwn/sr.ht) for issues that fit nowhere else,
or that you're unsure of the appropriate place to report your bug:


# Security vulnerabilities

Please send an email to [~sircmpwn/sr.ht-security@lists.sr.ht
](mailto:~sircmpwn/sr.ht-security@lists.sr.ht). Please do not discuss security
vulnerabilities in public until a fix has been developed and deployed.
