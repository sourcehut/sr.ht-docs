---
title: Configuration
---

This document provides general information about the configuration process of
sr.ht services.

Similar to the installation process, configuring sr.ht services typically
involves steps that are specific each service. That being said, they all share
certain aspects which are described below. Be sure to take a look at each
service's configuration page for more details:

- [builds.sr.ht](/builds.sr.ht/configuration.md)
- [git.sr.ht](/git.sr.ht/configuration.md)
- [hg.sr.ht](/hg.sr.ht/configuration.md)
- [lists.sr.ht](/lists.sr.ht/configuration.md)
- [man.sr.ht](/man.sr.ht/configuration.md)
- [meta.sr.ht](/meta.sr.ht/configuration.md)
- [todo.sr.ht](/todo.sr.ht/configuration.md)

# Prerequisites

Before configuring any sr.ht service, ensure that you have the following
services installed. Every sr.ht service makes use of them, so your deployment
will likely not work as expected if they are missing.

- [core.sr.ht][core] — Provides functionality used by all sr.ht services.

    - For users installing from packages, this package is automatically pulled
      in as a dependency.

    - For users installing from source, this package must be built *first*.

        <div class="alert alert-info">
          <strong>Note:</strong> The dependencies in <code>setup.py</code> may
          not be up-to-date. For an up-to-date list of dependencies, consult
          the latest
          <a href="https://git.sr.ht/~sircmpwn/sr.ht-apkbuilds/tree/master/sr.ht/py3-srht/APKBUILD"
             class="alert-link">Alpine package</a>.
        </div>

- [meta.sr.ht][meta] — Performs various administrative tasks (e.g., storing
  account details, handling logins, SSH and PGP key storage).

[meta]: https://git.sr.ht/~sircmpwn/meta.sr.ht
[core]: https://git.sr.ht/~sircmpwn/core.sr.ht

# Files

## `config.ini`

sr.ht services all use a shared configuration file, located at
`/etc/sr.ht/config.ini`. The specific options you will configure will depend on
the subset of and configuration of sr.ht services you choose to run.

Each service provides an example configuration file, called
`config.example.ini`, in their source code repository, with annotations
explaining each configuration option. You should consult these when creating
your unified `config.ini`.

## Service Files

Service files for daemons are pre-configured by their distribution packages.
After setting up your `config.ini` appropriately, you may start them using your
distribution's service manager (e.g., `service git.sr.ht start`).

The list of daemons available for each service are documented on their
respective installation page.

# Web services

Most sr.ht services include a web component, and the standard installation
procedure requires you to configure a reverse proxy to serve them. A typical
nginx configuration may look something like this:

```
server {
    listen 80;
    server_name meta.sr.ht;

    location / {
        return 302 https://$server_name$request_uri;
    }

    location ^~ /.well-known {
        root /var/www;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name meta.sr.ht;
    client_max_body_size 100M;
    ssl_certificate /etc/ssl/uacme/meta.sr.ht/cert.pem;
    ssl_certificate_key /etc/ssl/uacme/private/meta.sr.ht/key.pem;

    location / {
        proxy_pass http://127.0.0.1:5000;
    }

    location /query {
        proxy_pass http://127.0.0.1:5100;
    }

    location /static {
        root /usr/lib/python3.8/site-packages/metasrht;
    }
}
```

Note the following requirements:

- Proxy `/` to the web application
- Serve static assets from `/static` (optional, but recommended)
- For many services, `/query` should be proxied to the API service

See [sr.ht-nginx](https://git.sr.ht/~sircmpwn/sr.ht-nginx) for the nginx
configurations we use in production.

# Databases

Each service requires its own database. It is recommended to give each service
its own database login, with full permissions for its database so that it may
manage its own schema migrations.

Once you populate the `connection-string` field in your `config.ini`, you may
use the `schema.sql` file to populate the database (e.g.
`psql -d meta.sr.ht -f schema.sql` to set up meta.sr.ht's database).

Note that if you have not configured SSL for your PostgreSQL server, you may
have to append `?sslmode=disable` to your connection string for some services
to work.

## Schema Upgrades

We use [alembic](https://alembic.zzzcomputing.com/en/latest/) to manage schema
migrations. We use custom scripts to automatically retrieve database
credentials from your `config.ini`. If you have installed from distribution
packages, set `[<service>] migrate-on-upgrade=yes` to have migrations
automatically performed during normal service upgrades (e.g., `[meta.sr.ht]
migrate-on-upgrade=yes`).

Otherwise, you may use `srht-migrate <service> upgrade head` to run updates for
core.sr.ht migrations, and `<service>-migrate upgrade head` to run
service-specific upgrades. For example, to upgrade the database schema for
git.sr.ht, run `srht-migrate git.sr.ht upgrade head`, then `gitsrht-migrate
upgrade head`. Other alembic commands are available, use `gitsrht-migrate
--help` for more information.

# Upgrade Procedure

1. Stop all services.
2. Update and upgrade your packages (e.g. `apk update && apk upgrade`).
3. Resume all services.

If you have high availability requirements, the web services may be
load-balanced to avoid an outage.

Database migrations will be performed automatically during upgrades. We source
your alembic config from your main `config.ini`, so there's no need to write an
`alembic.ini` file.

Run `srht-migrate <service> stamp head && <service>-migrate stamp head` once to
tell alembic the schema is up-to-date (e.g. `srht-migrate man.sr.ht stamp head
&& mansrht-migrate stamp head`).  Future upgrades will be managed automatically
by the package, or if you're using the source code you can run `srht-migrate
<service> upgrade head && <service>-migrate upgrade head` when you pull the
latest version down.

# Integrations

Many sr.ht services are able to integrate with third-party tools, such as:

- [Prometheus](https://prometheus.io/) for monitoring.
- Any S3-compatible object storage (e.g., [Minio](https://min.io/),
  [Ceph](https://ceph.io/)).
