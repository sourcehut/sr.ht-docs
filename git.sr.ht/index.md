---
title: git.sr.ht docs
---

[git.sr.ht](https://git.sr.ht) is a service for hosting git repositories on
sr.ht.

**See also**:

- [GraphQL reference](graphql.md)
- [Installation guide](installation.md)

# New to git?

Learning how to use git in general is out of the scope of our documentation.
Here are some good resources:

- [The official Git book](https://git-scm.com/book/en/v2)
- [Git man pages](https://git-scm.com/docs)

We do have some general resources for learning how to use git the sr.ht way:

- [Using git-send-email for sending and reviewing patches on sr.ht](/git.sr.ht/send-email.md)
- [Code review with lists.sr.ht](/lists.sr.ht/code-review.md)

# SSH host keys

If you are cloning via ssh, these lines should be added to
`~/.ssh/known_hosts`.

```
git.sr.ht ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ+l/lvYmaeOAPeijHL8d4794Am0MOvmXPyvHTtrqvgmvCJB8pen/qkQX2S1fgl9VkMGSNxbp7NF7HmKgs5ajTGV9mB5A5zq+161lcp5+f1qmn3Dp1MWKp/AzejWXKW+dwPBd3kkudDBA1fa3uK6g1gK5nLw3qcuv/V4emX9zv3P2ZNlq9XRvBxGY2KzaCyCXVkL48RVTTJJnYbVdRuq8/jQkDRA8lHvGvKI+jqnljmZi2aIrK9OGT2gkCtfyTw2GvNDV6aZ0bEza7nDLU/I+xmByAOO79R1Uk4EYCvSc1WXDZqhiuO2sZRmVxa0pQSBDn1DB3rpvqPYW+UvKB3SOz
git.sr.ht ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCj6y+cJlqK3BHZRLZuM+KP2zGPrh4H66DacfliU1E2DHAd1GGwF4g1jwu3L8gOZUTIvUptqWTkmglpYhFp4Iy4=
git.sr.ht ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMZvRd4EtM7R+IHVMWmDkVU3VLQTSwQDSAvW0t2Tkj60
```

# Acceptable resource usage

The guidelines on resource usage are somewhat flexible. Here are some tips:

- As an approximation, aim to have repos no larger than the Linux kernel
  git repo, which is 3.5 GiB.
- Don't store lots of large binary objects in your repository, such as images,
  audio, or video. A couple of small binary files (such as your logo or a
  screenshot) is fine.

If in doubt, you're probably fine. We monitor resource usage and we'll send you
a friendly email if we notice that you're using too much.

## Automated use to git operations

If you are performing git operations (clone, fetch, pull, push, etc) in bulk in
an automated fashion, we kindly ask that you limit your git operations to once
per minute. We also appreciate it if you set the `GIT_HTTP_USER_AGENT`
environment variable to something useful, including your name or your project
name and some manner of reaching you (e.g. an email address).

If you are polling git.sr.ht repositories to check for updates, we ask that you
consider using GraphQL webhooks instead. Anyone can create a GraphQL webhook for
any repository you can ordinarily access (e.g. a public repo), you do not have
to be a repository owner to do so. This allows git.sr.ht to contact *you* when a
repository you're interested in is updated. We'll even send you a diff of the
changes. [Documentation available here](https://man.sr.ht/graphql.md#webhooks).

# Sending patches upstream

git.sr.ht provides a web-based patch preparation UI, which you can use to
prepare changes to send upstream online. You can even use this to prepare
patches for projects that use email for submission, but are not hosted on
SourceHut. This tool may be helpful to users who are used to the "pull request"
style contribution popularized by GitHub, GitLab, and others.

<div class="alert alert-info">
  <strong>Heads up:</strong> git also has a built-in CLI tool for preparing
  patches to send upstream. <a href="https://git-send-email.io">Check out our
  tutorial here</a> if you prefer to work from the command line.
</div>

## 1. Getting the source onto git.sr.ht

If the project is already on git.sr.ht, look for the large blue "Clone repo to
your account" button to add a copy of it to your git.sr.ht repos. If the project
is hosted elsewhere, clone the upstream source code and [add it to git.sr.ht
like any other repository](/tutorials/set-up-account-and-git.md).

Write your changes locally with your normal workflow. We recommend studying [git
rebase](https://git-rebase.io) as well, because many projects which use the
email-based workflow may expect you to be proficient with it. When you've
completed your work, push it to your git.sr.ht repository.

## 2. Preparing the patchset

On the git.sr.ht website, visit your repository's summary page, and the large
blue button is replaced with "Prepare a patchset". Click this to start up the
patchset editor.

On the first page, you'll select the *oldest* commit to include in the
patchset. Click "Continue" to select the *newest* commit to include &mdash; the
default is probably correct. On this second page, you can add some notes to the
patchset. Here's an explanation of the options:

**Add a cover letter**

A cover letter is added ahead of your patchset. It includes a brief
auto-generated summary of the changes, and gives you an opportunity to add any
notes or set a subject line for the whole patchset. If you're coming from
GitHub, think of this as setting the pull request title and description.

**Add commentary**

For each commit that you include, you have a space to add some notes specific to
that commit. The notes you add here supplement the commit message with
additional details which are useful for the reviewer, but shouldn't be
immortalized in the commit log.

**Patchset revision**

After you receive feedback (see below), you'll want to update this number to
reflect how many revisions you've sent upstream. You can leave this at "1" for
the first submission.

## 3. Sending it upstream

After reviewing all of the details, click "Continue" to proceed to the final
step. You'll have to figure out who the emails should be sent to before
completing this step. Most projects have a mailing list address listed in their
contributor documentation &mdash; find that and stick it in the "To" box. You
might also want to use tools like [git blame](https://www.git-scm.com/docs/git-blame)
to find other people who have recently worked in relevant parts of the code, and
add them to the "Cc" field.

Have one more quick look over the emails that have been prepared for you (you
can press "back" in your browser to revise any mistakes), then hit "Send
patchset" to send your work to the email addresses you've listed. You're done!

## 4. How to deal with feedback

When the reviewers look over your work, they may have some comments for things
that you should change or clarify. Comments will arrive at your email address,
and you can simply reply to them to answer their questions or provide follow-up
questions of your own. Please double-check our [email etiquette
guide](https://man.sr.ht/lists.sr.ht/etiquette.md) to avoid any faux pas!

To incorporate their feedback, you'll want to edit the commits in your git
repository. Note that you need to *edit* your commits &mdash; you can't just add
new commits which fix the issues introduced by earlier commits. Check out our
[git rebase](https://git-rebase.io) guide if you're not comfortable with this
workflow. Once you're satisfied with your changes, use `git push -f` to push the
new commits up to your repository on git.sr.ht, and follow this procedure again
to send the new version. Update the **patchset revision** field on step two when
you get there &mdash; set it to the number of patchset versions you've sent so
far.

# Referencing tickets in git commit messages

If your source repository is linked to a [project](/hub.sr.ht/index.md), you
may interact with [todo.sr.ht](/todo.sr.ht/index.md) tickets via specific git
commit [trailers](https://git-scm.com/docs/git-interpret-trailers). The trailer
values must be valid URLs to tickets. Any other value will be ignored.
A comment will be inserted in the referenced tickets with a back-link to the
git commit and its original author.

For example, the following commit message:

```
build: work around gcc 4.2 bug

Fix build with ancient gcc.

Fixes: https://todo.sr.ht/~arkanoid/foobar/666
Signed-off-by: John Doe <john@doe.io>
```

Will cause the following change on the referenced issue:

> [~arkanoid](https://sr.ht/~arkanoid) **UNRESOLVED -> FIXED** _9 seconds ago_
>
> _John Doe referenced this ticket with commit
> [badcaca0](https://git.sr.ht/~arkanoid/foobar/commit/badcaca0)_.

<div class="alert alert-info">
  <strong>Note:</strong>
  The user pushing the commit must have comment and triage access to the bug
  tracker, otherwise the trailers will be ignored. The commit author is only
  used in the inserted comment.
</div>

<div class="alert alert-warning">
  <strong>Attention:</strong>
  Only the last 25 commits will be considered when pushing large series. Any
  referenced issue in a commit preceding the last 25 will be ignored. If you
  <i>really</i> need to push more than 25 commits for a single feature, do
  it in multiple pushes.
</div>

The following trailers are supported:

## Fixes

If the ticket is unresolved, it will be resolved with the `FIXED` resolution.
If it is already resolved, only a comment will be inserted.

Example:

```
build: work around gcc 4.2 bug

Fix build with ancient gcc.

Fixes: https://todo.sr.ht/~arkanoid/foobar/666
Signed-off-by: John Doe <john@doe.io>
```

## Implements

If the ticket is unresolved, it will be resolved with the `IMPLEMENTED`
resolution. If it is already resolved, only a comment will be inserted.

Example:

```
foo: add support for yaml config files

Implements: https://todo.sr.ht/~arkanoid/foobar/1337
Signed-off-by: John Doe <john@doe.io>
```

## References

The ticket status will not be changed, only a comment will be inserted.

Example:

```
foo: add missing docs for yaml config

References: https://todo.sr.ht/~arkanoid/foobar/1337
Signed-off-by: John Doe <john@doe.io>
```

## Closes

If the ticket is unresolved, it will be resolved with the neutral `CLOSED`
resolution. If it is already resolved, only a comment will be inserted.

# Attaching files to releases

git.sr.ht allows you to attach files, such as executables (aka binaries), PGP
signatures, and so on, to *annotated tags*. To create an annotated tag, run the
following git command:

```
git tag -a <tag name>
```

For example, `git tag -a 2.3.4` to tag version 2.3.4. Your text editor will
open, and you'll be prompted to *annotate* the tag — fill this in with release
notes, a changelog, etc. Consider using
[`git-shortlog`](https://git-scm.com/docs/git-shortlog) to generate your
changelog.

Save and close your editor, then use `git push --tags` to publish the new tag
or use `git push --follow-tags` to push any local commits to the current
branch, along with the new tag (this can be made the default behavior by running
`git config --global push.followTags true`). The new tag will appear on the
"refs" page of your repository. To attach files to it, click the tag name (e.g.
"2.3.4") and use the upload form on this page.

# Signing tags' tarballs

In addition to serving tarballs for tags,
git.sr.ht may also serve PGP signatures for those tarballs with `.asc` appended.

This is done by storing the signatures as notes in the
`refs/notes/signatures/tar{,.gz}` namespaces.

As an example, the following shell program will sign the given tags
in the given format:

```sh
[ $# -lt 1 ] && { echo "usage: $0 tar|tar.gz tag..." >&2; exit 1; }
fmt=$1; shift
repo=$(git remote get-url origin)
for tag; do
  git -c tar.tar.gz.command='gzip -n' archive --format "$fmt" --prefix "${repo##*/}-$tag/" "$tag" |
    gpg --detach-sign -a |
    git notes --ref refs/notes/signatures/"$fmt" add -F- "$tag"
done
git push origin refs/notes/signatures/"$fmt"
```

Be 'ware of compressor variance: you must compress with `gzip -n`!
If signatures for both `tar` and `tar.gz` are set, `tar` wins.

# Push Options

git.sr.ht supports some git push options, which can be specified with `-o
option` or `-o option=value`.

- **debug**: prints the UUID assigned to your git push. You may be asked to
  provide this when troubleshooting push issues.
- **skip-ci**: skips submitting builds.sr.ht jobs for this push.
- **submit**: overrides the default comma-separated
  [`fnmatch(3)`](https://manpages.debian.org/buster/manpages-dev/fnmatch.3.en.html)
  pattern for build manifests to submit (`.build.yml,.builds/*.yml`).
- **description**: set the repository's description.
- **visibility**: set the repository's visibility (`public`, `unlisted`, or `private`).

All of your push options, including ones not recognized by git.sr.ht itself, are
forwarded to any webhooks you have configured for your repository.

To set any of these options permanently for a specific git repository, use the
following command:

```
git config --add push.pushOption submit=".sourcehut/*.yml"
```

# Changing the default branch

If you wish to change your default branch, visit the settings tab of your
repository. To rename your default branch, use something like the following:

    git branch -m master main
    git push origin :master main:main

# README and LICENSE files

You may create a README file for your repository which will be displayed on the
repo summary page, and on a [project hub](https://sr.ht) project if you list
your project there. The following file names are accepted:

- README
- README.md
- README.markdown

The latter two options are rendered as markdown. You may use other formats by
preparing a custom readme, see the next section for details.

You are also encouraged to add a LICENSE file which details the use and
distribution terms of your software. The following names are accepted:

- LICENSE
- COPYING
- COPYRIGHT
- LICENSES

If your project has more than one license, your license file should explain what
licenses apply to what parts of the code in plain English, and include the text
of all applicable licenses.

For cases like this, the git.sr.ht web UI supports
version 3.0 of the [REUSE Specfication](https://reuse.software/spec/):

If you include your licences as files matching the pattern
`LICENSES/$SPDX.$suffix`, where `$SPDX` is the
[SPDX identifier](https://spdx.org/licenses/) for the licence
and `$suffix` is any string without a dot,
the licences will be enumerated in the repo summary
and `/licenses` pages, alongside a short description and links
to their corresponding entries on SPDX.org.

## Setting a custom README

By default, if found, a `README` plaintext or `README.md` markdown file
will be rendered as the repository's README.

However, you can use an arbitrary HTML snippet as your README instead by using
the [GraphQL API](https://man.sr.ht/graphql.md).

[hut](https://sr.ht/~xenrox/hut/) can easily do this:

```
hut git update --readme readme.html --repo https://git.sr.ht/~foo/bar
```

Or if you want to remove the custom README:

```
hut git update --readme "" --repo https://git.sr.ht/~foo/bar
```

It may be desirable to configure a builds.sr.ht job to compile your README from
another markup format and submit it on each git push.
Check out some external guides for more information:

<table>
    <tbody>
        <tr>
            <td>HTML</td>
            <td><a href="https://git.xenrox.net/~xenrox/custom-readme">example readme repo</a></td>
        </tr>
        <tr>
            <td>Org mode</td>
            <td><a href="https://breatheoutbreathe.in/blog/2024-12-10-sourcehut-org-mode-readme.html">external blog post</a></td>
        </tr>
    </tbody>
</table>
