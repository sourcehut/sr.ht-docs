---
title: git.sr.ht Installation
---

This document covers the installation steps for
[git.sr.ht](https://git.sr.ht/~sircmpwn/git.sr.ht), a git repository hosting
service.

# Installation

git.sr.ht can be installed through [package
installation](/installation.md#installing-from-packages).

## Daemons

- `git.sr.ht` — The web service.
- `git.sr.ht-api` — The API service.
- `git.sr.ht-webhooks` — Webhook delivery service.

## Cronjobs

- `gitsrht-periodic` — Performs various maintenance tasks.

## Configuration

See [Configuration](configuration.md).
