---
title: git.sr.ht Configuration
---

This document covers the configuration process for git.sr.ht.

# Cronjobs

- `gitsrht-periodic`: The recommended configuration is
  `*/20 * * * * gitsrht-periodic`.

# Storage

## Repository

<div class="alert alert-info">
  <strong>Note:</strong> If git.sr.ht was installed in a package, you may skip
  this section.
</div>

As a repository hosting service, git.sr.ht requires a place for storing
repositories (we recommend `/var/lib/git/`). It also requires a `git` user who
has ownership over the repository storage location.

## Objects

To allow users to upload artifacts to git repositories, an S3-compatible object
storage system may be set up and configured (separately from the repository
storage) before filling out the S3-related configuration options in your
`config.ini`.

<div class="alert alert-danger">
  <strong>Warning:</strong> You must secure the S3 storage to protect from
  unauthorized downloads of artifacts within private repositories. git.sr.ht
  will stream artifact downloads directly from the S3 storage after confirming
  authorization, so you simply need to avoid configuring the bucket for public
  access.
</div>

<div class="alert alert-info">
  <strong>Note:</strong> For object storage, we recommend
  <a href="https://min.io" class="alert-link">MinIO</a>,
  a free and open-source S3-compatible storage server.
</div>

# SSH Dispatch

It is necessary to configure git.sr.ht's SSH dispatcher as the system-wide SSH
authorization hook. In `/etc/ssh/sshd_config`, configure gitsrht-dispatch like
so:

    AuthorizedKeysCommand=/usr/bin/gitsrht-dispatch "%u" "%h" "%t" "%k"
    AuthorizedKeysCommandUser=root
    PermitUserEnvironment SRHT_*

`sshd` will invoke our dispatcher whenever a connection is made to the server
to obtain a list of authorized keys for the connecting user. The default
behavior is to read the `.ssh/authorized_keys` file from that user's HOME
directory, but the dispatcher can also "dispatch" to other authentication tools
for other users. This is used to authorize and perform git operations via the
`gitsrht-keys` and `gitsrht-shell`. See the `[dispatch]` section of your
git.sr.ht configuration for details on how this works and how to configure it
for additional services (e.g. man.sr.ht).

Authorization logs are written to `/var/log/gitsrht-dispatch` and
`gitsrht-shell`.

If you have any issues with dispatch, please make sure the `git` user is not
locked by setting a password for it, and also make sure you can otherwise SSH
into it.

# HTTP(S) Cloning

git.sr.ht does not handle HTTP(S) cloning for you, so you'll need to set it up
yourself with your web server. Here's an example Nginx configuration:

```nginx
location = /authorize {
    proxy_pass http://127.0.0.1:5001;
    proxy_pass_request_body off;
    proxy_set_header Content-Length "";
    proxy_set_header X-Original-URI $request_uri;
}

location ~ ^/([^/]+)/([^/]+)/(HEAD|info/refs|objects/info/.*|git-upload-pack).*$ {
    auth_request /authorize;
    root /var/lib/git;
    fastcgi_pass unix:/run/fcgiwrap.sock;
    fastcgi_param SCRIPT_FILENAME /usr/lib/git-core/git-http-backend;
    fastcgi_param PATH_INFO $uri;
    fastcgi_param GIT_PROJECT_ROOT $document_root;
    fastcgi_param GIT_HTTP_EXPORT_ALL "";
    include fastcgi_params;
    gzip off;
}
```
<div class="alert alert-info">
  <strong>Note:</strong> On some systems (e.g. Alpine Linux), `SCRIPT_FILENAME`
  should be set to `/usr/libexec/git-core/git-http-backend`.
</div>

It is important that you set up the `/authorize` endpoint to enforce the
privacy of private repositories.

If you don't have `/run/fcgiwrap.sock` on your system, you'll need to install
the `fcgiwrap` package.

<div class="alert alert-info">
  <strong>Note:</strong> On some systems, the socket might be called
  `/run/fcgiwrap.socket`, `/run/fcgiwrap/fcgiwrap.sock`, or something else
  entirely. Consult your distribution's documentation.
</div>
