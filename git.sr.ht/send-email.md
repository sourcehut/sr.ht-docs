---
title: git send-email
---

sr.ht leverages git's built-in collaboration tools for contributing to projects
hosted here. This guide will help you get started.

# For contributors

This horrible wall of text has been replaced with a much better interactive
tutorial on using git send-email to send patches by email. Check it out here!

https://git-send-email.io

# For maintainers

## Tell people how to contribute

The first thing you need to do is help potential contributors figure out how to
contact you. The easiest way is to do nothing — git records your email with
every commit, so someone with a copy of your git repository can figure out how
to contact you. You'll probably want to make it a bit easier on them, though.

We recommend setting up a mailing list on [lists.sr.ht](/lists.sr.ht) for this
purpose. Once you do, you will get an email address for contributors to submit
patches to. Write this into your docs! You will probably also want to link to
the archives so that potential contributors can read other people's work to get
a feel for your submission process.

## Reviewing patches

When a patch comes in, you should review it carefully. Read the code, apply the
patch locally and make sure it compiles, test the changes, and so on. During
this process you'll probably come up with feedback on the patch. Pull open that
email client and compose a reply to the patch author. When your client composes
the reply, don't be afraid to slice and dice the email you're replying to — trim
out the fat and only include specific lines that you want to comment on.

If you only have small comments here and there, feel free to make the changes
yourself, again utilizing [`git commit --amend`][git-commit-amend] and [`git
rebase`][git-rebase] to your heart's content. You may be wise to point out these
small errors when you thank the submitter for their patch, however, if you don't
want to see the same errors in future patches.

## Applying patches

In order to integrate the changes, you need to *apply* the patch. The tool for
this is [`git am`][git-am]. The difficult part here is going to be obtaining a
copy of the email to provide to `git am`. Some clients like [mutt][mutt] make
this easy (in mutt, you can use the `|` key to pipe an email directly to `git
am`), or tools like [offlineimap][offlineimap] can help (or a combination of the
two!). Most popular end-user clients do not provide this option. If you're in
this boat, the easiest way to get a raw email is to use the "raw" link on
lists.sr.ht, which is hidden away under the "details" button.

[git-am]: https://www.git-scm.com/docs/git-am
[git-commit-amend]: https://www.git-scm.com/docs/git-commit#Documentation/git-commit.txt---amend
[git-rebase]: https://www.git-scm.com/docs/git-rebase
[mutt]: http://www.mutt.org
[offlineimap]: http://www.offlineimap.org/

If you copy the link to the raw email from lists.sr.ht, the command might look
like this:

    curl -s https://lists.sr.ht/... | git am

You can also just run `git am` alone and paste the patch into it, followed by
Ctrl+D. You can then make these commits available upstream by using [`git
push`][git-push] normally. Don't forget to send the contributor a thank you
email!

[git-push]: https://www.git-scm.com/docs/git-push
