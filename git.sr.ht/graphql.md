---
title: git.sr.ht's GraphQL API
toc: false
---

git.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [git.sr.ht GraphQL playground](https://git.sr.ht/graphql)
- [git.sr.ht GraphQL schema](https://git.sr.ht/~sircmpwn/git.sr.ht/tree/master/item/api/graph/schema.graphqls)
