---
title: General resources
---

Here are some general resources about sr.ht as a whole.

# How do you write/pronounce "sourcehut"?

First of all, the grammar police have been instructed not to intervene in any
crimes related to the spelling or pronounciation of "sourcehut" or "sr.ht". If
you have a way you like, you keep doing you.

"sourcehut", "SourceHut", or "Sourcehut" are the preferred spellings;
"SourceHut" is recommended if you wish to capitalize it at the start of a
sentence.

"sr.ht" is pronounced "sourcehut". The dot is silent and the vowels are implied.
"git.sr.ht" is pronounced "git sourcehut", and so on.

Pronouncing "sr.ht" as "shart" is discouraged.

# Billing

For information on billing, refer to the [billing FAQ](/billing-faq.md).

# Terms of Service

By using sr.ht, you agree to its [terms of service](/terms.md).

# Privacy Policy

We take your privacy extremely seriously. To learn more, review our [privacy
policy](/privacy.md).
