---
title: Choosing a software license
---

Most public projects on SourceHut (or anywhere else) need to have a software
license before others can use the project, contribute to the code, and so on.
Choosing the right license for your needs is important. If you're unsure of
which to use, we can offer some suggestions.

<div class="alert alert-info">
  Once you pick a license, add it to your project by copying the plain-text
  version into a file called "LICENSE" or "COPYING" at the root directory of
  your repository.
</div>

*Note: we are not your lawyer and this is not legal advice.*

## I want others to share their improvements with me.

The Free Software Foundation publishes the GNU General Public License for this
purpose. In short, it requires anyone who publishes modified versions of your
code or software that uses it to publish their changes or the software they
incorporated it into under the same license terms. Version 3 also requires
hardware manufacturers to allow users to install their modifications onto
devices which use GPLv3 licensed software.

[GPLv3 information](https://www.gnu.org/licenses/gpl-3.0.en.html) &mdash;
[GPLv3 plain text](https://www.gnu.org/licenses/gpl-3.0.txt)

If your software is a library, you may prefer to use LGPL. It provides similar
obligations for those who modify and re-distribute your library, but it doesn't
impose on the programs which depend on your code.

[LGPLv3 information](https://www.gnu.org/licenses/lgpl-3.0.en.html) &mdash;
[LGPLv3 plain text](https://www.gnu.org/licenses/lgpl-3.0.txt)

Authors publishing software which is useful over a network &mdash; databases,
web applications, and so on &mdash; should consider the "Affero" General Public
License, or AGPL, as well. The AGPL is similar to the GPL, but its terms also apply
when your software is used to provide a service over a network. Sourcehut itself is
distributed with the AGPLv3 license.

[AGPLv3 information](https://www.gnu.org/licenses/agpl-3.0.html)
&mdash; [AGPLv3 plain text](https://www.gnu.org/licenses/agpl-3.0.txt)

## I want a simple license with few obligations.

The MIT and BSD licenses allow anyone to use or modify your software for any
purpose, including to make changes or incorporate it into their own software
without releasing their source code. Both licenses are very short and easy to
understand.

[MIT license text](https://spdx.org/licenses/MIT.html)

[BSD 3-clause license text](https://spdx.org/licenses/BSD-3-Clause.html)

## I want to protect my company's trademarks and license my patents.

If you're publishing software as a business, Apache 2.0 is likely to be the most
appropriate license for your open-source project. It explicitly protects your
trademarks and includes a patent license for any patents which are applicable to
your project.

[Apache 2.0 information](https://www.apache.org/licenses/LICENSE-2.0.html)
&mdash; [Apache 2.0 plain text](https://www.apache.org/licenses/LICENSE-2.0.txt)

## Other resources

- [Joinup Licensing Assistant](https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-find-and-compare-software-licenses)
- [Choose a License from GitHub](https://choosealicense.com/)
- [Free Software Foundation recommendations](https://www.gnu.org/licenses/)
- [The Open Source Initiative](https://opensource.org/)
- [SPDX License Index](https://spdx.org/)
- [tl;dr Legal](https://tldrlegal.com/)
- [The complete guide for open sourcing video games](https://drewdevault.com/2021/03/23/Open-sourcing-video-games.html)
