---
title: ID unification migration plan
---

## Background

A coming update to SourceHut's internal design calls for the values of the ID
columns in our databases to be made uniform. Each database (e.g. for git, hg,
builds, etc) maintained its own copy of tables with shared information, sourcing
updates from meta.sr.ht via webhooks, but each of these rows had an ID specific
to each database. This change will make these IDs uniform across all services in
preparation for linking our services more tightly together through GraphQL
Federation.

This document outlines the extra steps required to perform this upgrade for
third-party instances.

## Upgrade process

Ensure meta.sr.ht is running version 0.60.5 or later. Then, for each service,
one at a time:

1. Disable automatic migrations
1. Shut off the service
1. Run upgrades
1. Run `$service-migrate upgrade head` (e.g. gitsrht-migrate) for each service
   and monitor the migration progress
1. Turn on the service
1. Re-enable automatic migrations if desired

**Note**: hub.sr.ht must be upgraded last, if present, and the other services
must be running during its upgrade.

### git.sr.ht and hg.sr.ht

Both git.sr.ht and hg.sr.ht cache user IDs and usernames in Redis. Run the
following command to clear the Redis cache after the services have been turned
off and before they are turned on again:

	redis-cli --scan --pattern "*.sr.ht.ssh-keys.*" | xargs redis-cli del

### pages.sr.ht

A special upgrade process is required for pages.sr.ht, which does not use the
same system for database migrations as other services. Perform the same steps as
described above, then run the scripts in [contrib][contrib] in the following
order:

- ./contrib/add\_user\_remote\_id.py upgrade
- ./contrib/use\_canonical\_user\_id.py upgrade

To downgrade, repeat these commands in the reverse order with "downgrade".

[contrib]: https://git.sr.ht/~sircmpwn/pages.sr.ht/tree/master/contrib

## Downgrade process

For each service (excluding meta.sr.ht), one at a time:

1. Shut off the service
1. Run `$service-migrate downgrade $version`, selecting $version from the list
   below, for each service
1. Downgrade the software
1. Turn on the service
1. Re-enable automatic migrations if desired

## Affected database schema revisions

Last schema revision prior to this change, for reference if downgrading:

- builds.sr.ht: f79186791a25
- git.sr.ht: 64fcd80183c8
- hg.sr.ht: bb87b52896ac
- hub.sr.ht: de4adc3cc306
- lists.sr.ht: d4b5b0b5c3f6
- man.sr.ht: 391282b09533
- todo.sr.ht: e1e2e901be0c
- meta.sr.ht: n/a
- pages.sr.ht: n/a

## Additional resources

- [Informal migration notes](https://paste.sr.ht/~sircmpwn/fa0768bf80a3f006b5efdbafadf9d28dc30e42cc)
- [Chat logs from #sr.ht.staff during migration](https://paste.sr.ht/~sircmpwn/f57fe097fcfc5ff1c349780b6aed2d385adfc3ab)
