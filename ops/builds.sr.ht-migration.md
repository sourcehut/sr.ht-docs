---
title: builds.sr.ht billing change planning
---

builds.sr.ht is going to require users to have a paid account in advance of the
larger [beta payment migration](/billing-faq.md).

# Background

builds.sr.ht is the most expensive service we provide to our users. It is also
the most easily misused: we have, on many occasions, had to reach out to users
who are not paying for their account and who are submitting an excessive number
of expensive jobs.

This has been exacerbated recently with a rise in cryptocurrency mining attacks
on builds.sr.ht. Malicious users have been deliberately submitting huge numbers
of jobs under dozens of frequently registered accounts and deliberately
circumventing our abuse detection to use as much of our resources as possible to
mine cryptocurrencies. This exhausts our resources and leads to long build
queues for normal users.

We have a multifaceted approach to dealing with this kind of abuse. We will not
go into detail in public on our anti-abuse measures: secrecy is required for
them to be effective. However, one easy measure we can do to cut this abuse down
substantially is to make builds.sr.ht a paid service.

All sr.ht users will ultimately be expected to pay for their account
([details](/billing-faq.md)). Making builds.sr.ht paid ahead of the rest helps
to fix some problems, free up our sysadmins to focus on development, and does
not substantially change the long-term planning.

# FAQ

All of the details in the [billing FAQ](/billing-faq.md) apply here.

## Will pricing change after the alpha?

Yes. The exact numbers have not been decided upon. There will be a generous
period of consultation with the community and advance warning before any pricing
changes take effect.

## I'm not sure I can afford it.

It is not our intention to price anyone out of using the service. If the minimum
fees are too high for your financial needs, then you can simply [send us an
email](mailto:sir@cmpwn.com) explaining your situation, and you will be issued
free service, no questions asked. sr.ht users from many walks of life have been
granted free service: students, users with issues using their currency of
choice, people between jobs, and so on.

## I represent an organization, how do we pay for builds.sr.ht?

The software implementation for user groups or organizations is prioritized for
the beta. In the meanwhile, utilize any workaround you wish: nominate one of
your members to host your resources on their account, or set up a dedicated
pseudo-account for the organization, or any other approach that suits your
needs. We will help you migrate to user groups once the feature is available.
[Shoot us an email](mailto:sir@cmpwn.com) to let us know about your workaround
and we'll make a note on your account.

Billing considerations for organizations will be available when the beta begins,
but we don't know what they'll look like yet. The final discussion is deferred
pending the final software design. Don't double-charge yourself: just pay on a
single account and let us know. If you set up a dedicated organization account,
and one or more of your members has a paid sr.ht account, let us know and we'll
link the billing so that you don't have to pay twice.

## What about builds submitted for patches?

Jobs submitted to run tests against patches sent to mailing lists
are submitted under the project owner's account.
This account is the one that needs to be paid &mdash; not the accounts
of those submitting patches or pull requests.

# Community discussion

The proposed changes have been put to the community for discussion, via IRC
discussions in the week of 2021-04-12; in the public Mumble meeting on
2021-04-16; and on [sr.ht-discuss][0].

[0]: https://lists.sr.ht/~sircmpwn/sr.ht-discuss/%3C32e85430-b650-4774-ad14-00688e473c5b%40benaaron.dev%3E

# Detailed migration plan

The plan is as follows:

1. All new users who register on or after 2021-05-01 will be required to pay for
   their account to use builds.sr.ht. Users registered before this date will not
   be affected immediately:
2. On the same date, existing users will have 30 days to convert to a paid
   account before they will no longer be able to submit builds. Any non-paying
   users who have submitted builds during the 30 days prior to 2021-05-01 will
   be notified of the impending change by email, and will be given an
   opportunity to ask questions of the admins prior to the changes coming into
   effect. Users with extenuating circumstances who require additional migration
   time will be accomodated.

Tasks:

- ☑ Update billing FAQ and write this document
- ☑ Prep software changes for builds.sr.ht
- ☑ Fix billing system messaging bugs
- ☑ Send emails to affected users
- ☑ Deploy software changes
- ☐ After 30 days: roll back temporary software changes
