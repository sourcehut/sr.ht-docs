# Service outage post-mortems

May this list never grow.

- [Main database server failure](/ops/outages/2023-01-11-unreachable-host.md)
- [Extended network outage](https://sourcehut.org/blog/2024-01-19-outage-post-mortem/)
