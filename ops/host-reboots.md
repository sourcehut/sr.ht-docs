---
title: Host reboots
---

Procedure for upgrading and rebooting hosts (alice*.sr.ht):

1. For critical services, transfer them to another host and flip DNS
2. Run upgrades
3. Shut down all VMs manually (this saves time snapshotting them)
4. Reboot
5. Bring up all VMs
6. Sanity check all VMs

## build hosts

For builds.sr.ht hosts:

1. Run updates
2. Stop builds.sr.ht-worker service
3. Keep an eye on `docker ps` and let running jobs drain
4. Reboot
5. Sanity check everything came up okay
