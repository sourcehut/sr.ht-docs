---
title: SQL migrations
---

Intended audience: someone who wants to change the database schema

SQL migrations are presently based on [Alembic][0], though we wish they weren't.

[0]: https://alembic.sqlalchemy.org/en/latest/

TODO: Migrate to some other solution ASAP

Each service has a wrapper script around alembic which is used to auto-configure
alembic per SourceHut requirements, named `$service-migrate`, such as
`listssrht-migrate`. `./$service-migrate --help` can get you oriented.

## schema.sql

`schema.sql` is the authoratitive database schema for each service. It should be
updated when writing a migration to reflect what the schema should look like
after the migration is complete. It is used when creating a new database.

## Stamping the head

Before you do anything else, if you created your database by importing
schema.sql, you need to initialize the alembic state. Run `./$service-migrate
stamp head` to "stamp" the database schema version with the "head" of the
migration list.

You can stamp any other version as well by specifying its version hash instead
of "head". Run `./$service-migrate history` to print a list of revisions
available.

## Creating a new migration

`./$service-migrate revision -m "short message describing changes"`

Then edit the file that appears, adding steps to upgrade and downgrade the
schema.

**Note**: we do not use any alembic features other than `op.execute` at this
time:

```
op.execute("""

ALTER TABLE ...

""")
```

## Applying migrations

`./$service-migrate upgrade head` will apply all revisions between whatever
revision was stamped two steps ago and the latest revision.

## Rolling back migrations

`./$service-migrate downgrade $target` will apply all downgrades between the
head and the $target revision. You should test that your downgrades also work
when working on your migration.
