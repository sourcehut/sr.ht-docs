---
title: GraphQL Federation roll-out plan
---

This document outlines the process in which SourceHut plans to roll out GraphQL
Federation.

1. Audit usage of user IDs across services

   As part of the GraphQL Federation roll-out, user IDs will need to be
   streamlined to match those of meta.sr.ht. Audit all usages of user IDs to
   ensure that none will be affected by this change.

1. Test user ID migration and rollback process

   Thoroughly test the migration and rollback process for streamlining user IDs
   so that the upgrade can be reverted in case of problems.

1. Migrate user IDs

   Once testing has been completed, proceed with the migration process.

1. Audit usage of GraphQL queries

   Audit the internal usage of GraphQL queries across services. These will need
   to be updated to use the gateway once GraphQL federation is rolled out.

1. Update services to support GraphQL federation

   meta.sr.ht, todo.sr.ht, and hub.sr.ht will be the first services to support
   GraphQL federation. This will involve the implementation of a new GraphQL API
   for hub.sr.ht.

1. Bring up the GraphQL API gateway

   The GraphQL API gateway will be brought online. Individual APIs for each
   service will remain accessible for the short term.

1. Redirect internal GraphQL queries to use the gateway

   Redirect any internal GraphQL queries which target meta.sr.ht or todo.sr.ht
   to target the API gateway instead. core.sr.ht's `srht.graphql` package can be
   modified to achieve this.

1. Sunset meta.sr.ht and todo.sr.ht APIs

   The meta.sr.ht and todo.sr.ht GraphQL APIs will no longer be accessible to
   the public. The hub.sr.ht GraphQL API should never be accessible in the first
   place. All queries for these services must go through the gateway.

1. Rewrite the hub.sr.ht frontend to use GraphQL

   hub.sr.ht is a good target for validating the design of the federated graph
   since it is a relatively simple service which aggregates data from other
   services. Rewrite its frontend to use GraphQL for all operations.

1. Update remaining services

   After validating the design of the gateway, slowly update more services to
   support GraphQL federation until all queries are made through the GraphQL
   gateway.
