---
title: "Server provisioning & allocation"
---

Standards for provisioning of VMs and physical hosts.

# Alpine Linux

Our standard loadout uses Alpine Linux for all hosts and guests.

- **TODO**: ns1 and ns2 are on Debian, they need to be reprovisioned (add an ns3
  while we're at it?)

# Physical hosts

## VM hosts

Our current VM hosts won't scale much further. We need to figure out a better
hardware loadout for these going forward.

## High performance VM hosts

For performance critical services (presently only git.sr.ht demands this), we do
have a standard loadout:

- AMD EPYC 7402 (24 cores, 48 threads)
- Micron 36ASF2G72PZ-2G6F1 RAM (4x, 64G total)
- 1x NVMe for host system, ext4; WD Black 1T
- 4x SSD on SATA, direct passthrough to guest

This is spec'ed to CPU and I/O intensive workloads.

## Build hosts

builds.sr.ht uses dedicated build runners. Our current standard is:

- AS-1013S-MTR SuperMicro barebones
- AMD EPYC 7281 (16 cores, 32 threads)
- M393A4K40CB2-CTD RAM (4x, 128G total)
- 1x NVMe for root, ext4; WD Black 1T
- 3x HDD on SATA for /var, ZFS; 1T each, various vendors/models

This configuration supports up to 16 parallel build slots.

# Virtual machines

There is no standard loadout — tune the specifications for the task at hand.
Generally limit 1 VM == 1 service, and tune accordingly.
