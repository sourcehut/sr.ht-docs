---
title: hg.sr.ht's GraphQL API
toc: false
---

hg.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [hg.sr.ht GraphQL playground](https://hg.sr.ht/graphql)
- [hg.sr.ht GraphQL schema](https://hg.sr.ht/~sircmpwn/hg.sr.ht/browse/api/graph/schema.graphqls?rev=tip)
