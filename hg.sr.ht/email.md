---
title: Sending emails with hg.sr.ht
---

sr.ht leverages mercurial's built-in collaboration tools for contributing to
projects hosted here. This guide will help you get started. If you run into any
trouble, please send an email to the [sr.ht-discuss][sr.ht-discuss] mailing list
for help.

[sr.ht-discuss]: https://lists.sr.ht/~sircmpwn/sr.ht-discuss

**Golden rule**: Do not copy-paste the output of `hg export` into your typical
mail client.

# For everyone

Before you dig too far into this guide, you should make sure that your email
client is configured to use plain text emails. By default, many email clients
compose emails with HTML, so you can use rich text formatting. Rich text is not
desirable for development-oriented email conversations, so you should disable
this feature and send your email as "plain text". Every email client is
different, you should research the options for your specific client. HTML emails
are rejected by all sr.ht services.

For real-world examples of how the discussions described in this document play
out, check out the [sr.ht-dev][sr.ht-dev] mailing list.

[sr.ht-dev]: https://lists.sr.ht/~sircmpwn/sr.ht-dev

# For contributors

## Preparing your changes

There's no need to "fork" the repository you want to contribute to — simply use
[`hg clone`][hg-clone] to obtain a local copy of the mercurial repository and
[work normally][work-normally]. Be deliberate about your commits — use
meaningful commit messages and take special care to commit your work in the form
of logically separate changes. When it comes time to review your work, your
commit history is an important tool for the reviewer and will be closely
examined.

[hg-clone]: https://www.mercurial-scm.org/doc/hg.1.html#clone
[work-normally]: https://book.mercurial-scm.org/read/

# Find out where to send your changes

This workflow is optional for projects hosted on sr.ht and each project will
have different requirements — review them carefully. To use this guide, you need
to find an email address to send your work to — this will often be a mailing
list on [lists.sr.ht](/lists.sr.ht). You will also want to find people who can
help review your changes — look for dedicated maintainers for the modules you're
working on, or use [`hg annotate`][hg-annotate] to find people who have recently
worked on similar code.

[hg-annotate]: https://www.mercurial-scm.org/doc/hg.1.html#annotate

# Configure hg email

When you've collected a list of email addresses to send your work to, we can use
[`hg email`][hg-email] to do the job. Its purpose is to convert your mercurial
commits into emails with [`hg export`][hg-export] and connect to your mail
server to deliver them with SMTP.

[hg-email]: https://www.mercurial-scm.org/doc/hg.1.html#email
[hg-export]: https://www.mercurial-scm.org/doc/hg.1.html#export

If you've never used hg email before, you will need to do some one-time setup to
enable the `email` command (which isn't accessible by default, but is available
in the `patchbomb` extension which ships with Mercurial), and introduce it to
your SMTP server. The connection details vary between mail providers, but you're
looking for information which is suitable for filling out these config fields in
[mercurial's configuration file][hgrc]:

[hgrc]: https://www.selenic.com/mercurial/hgrc.5.html

    [extensions]
    patchbomb =

    [smtp]
    host = mail.example.org
    port = 587
    tls = smtps
    username = you@example.org

You can also set your SMTP password as `smtp.password`. If you don't, you
will be prompted for it when it's needed.

## Patch Formatting on [lists.sr.ht][lists.sr.ht]

Patches sent to [lists.sr.ht][lists.sr.ht] are only recognized when using [git
style][git style] diffs. This must be configured with [mercurial's
configuration file][hgrc]:

    [diff]
    git = 1

[lists.sr.ht]: https://lists.sr.ht/
[git style]: https://www.mercurial-scm.org/wiki/GitExtendedDiffFormat

## Send the patches along

When you've configured `hg email`, completed your work, and you're ready to send
your patches in, you can run `hg email -r [rev]`. The `[rev]` here is the same
as any other [revision number, identifier, or name][hg-rev]. The command will
prepare a patch for that commit and send it through your mail server.

A few things of note:

- `hg email -o [dest]` will prepare patches for any revision not already in the
  upstream `[dest]` repository (_i.e._ anything that would show up with `hg
  outgoing [dest]`). If you have configured the `default` destination (in the
  `[paths]` section), you can even just do `hg email -o`!
- `hg email -r tip` includes the last commit.
- You can use [revsets][hg-rev] to specify revision ranges, like, say, `hg
  email -r 3000:3005` for sending patches for revisions 3000 through 3005.
- Add the `--confirm` option to give you an extra step to catch mistakes.

[hg-rev]: https://www.mercurial-scm.org/doc/hg.1.html#specifying-revisions

Mercurial might prompt you for more information before sending the email. For
instance, it might ask you what your email address is. Although it uses what it
found in your config's `ui.username`, it wants to be sure. You can skip that
step by setting the `email.from` option in your config too:

    [email]
    from = Your Name <you@example.org>

If you're sending more than one patch at once, Mercurial will ask you to write
an introduction email to the patch series. In many cases, this is unnecessary,
so you can optionally disable this feature in your config (you can always pass
`--intro` to `hg email` if you want it back):

    [patchbomb]
    intro = never

## Handling feedback

You will likely receive replies to your email with feedback on your changes.
This is normal! Use tools like [`hg commit --amend`][hg-commit] and [`hg
rebase`][hg-rebase] (which, again, isn't accessible by default but is available
in the `rebase` extension which ships with Mercurial) to continue improving
your patch set and iterating on feedback. When you're ready to submit the next
version of your patches, use `hg email` normally, except:

- Add `--flag V2` to indicate that this is version 2 of your patch (or whatever
  number is appropriate).
- Optionally, add `--in-reply-to [msgid]`, where `[msgid]` is the message ID of
  the last email in the thread. On lists.sr.ht you can get this by clicking
  "details" on the email in question. If you can't find this, don't sweat it,
  it's no big deal.

[hg-commit]: https://www.mercurial-scm.org/doc/hg.1.html#commit
[hg-rebase]: https://www.mercurial-scm.org/doc/hg.1.html#rebase

## Pulling from upstream

As you continue to work, you may want to pull from the upstream, and you almost
certainly don't want to create a merge commit when you have work in progress or
unmerged changes in your history. To this end, you should generally use `hg pull
--rebase` (this option is only available once the `rebase` extension is
installed) to fetch the latest changes from upstream.

## Extra tips

Here are a few extra tricks you might find useful with `hg email`.

### Sending emails to the same address every time

If you send emails for a project to the same mailing list every time, you might
find it useful to set the default destination address. Edit the project's
repository config (`.hg/hgrc`) with:

    [email]
    to = patches@example.org

### Specifying a subproject for shared lists

Some projects have several repositories being discussed on a single mailing
list, and it's often helpful to specify the particular repository your patch
pertains to.

If you're just doing this once, add `--flag` to `hg email`. You can specify it
multiple times if you also need to specify `--flag V2`. For instance:

    hg email --flag 'example' -o

You can also specify this as the default for that mercurial repository by
editing its config (`.hg/hgrc`):

    [patchbomb]
    flagtemplate = {separate(' ', 'example', flags)}

This will build a default `--flag` starting with "`example`" and followed by
any other value passed through the command-line.

# For maintainers

## Tell people how to contribute

The first thing you need to do is help potential contributors figure out how to
contact you. The easiest way is to do nothing — mercurial records your email
with every commit, so someone with a copy of your mercurial repository can
figure out how to contact you. You'll probably want to make it a bit easier on
them, though.

We recommend setting up a mailing list on [lists.sr.ht](/lists.sr.ht) for this
purpose. Once you do, you will get an email address for contributors to submit
patches to. Write this into your docs! You will probably also want to link to
the archives so that potential contributors can read other people's work to get
a feel for your submission process.

## Reviewing patches

When a patch comes in, you should review it carefully. Read the code, apply the
patch locally and make sure it compiles, test the changes, and so on. During
this process you'll probably come up with feedback on the patch. Pull open that
email client and compose a reply to the patch author. When your client composes
the reply, don't be afraid to slice and dice the email you're replying to — trim
out the fat and only include specific lines that you want to comment on.

If you only have small comments here and there, feel free to make the changes
yourself, again utilizing [`hg commit --amend`][hg-commit] and [`hg
rebase`][hg-rebase] to your heart's content. You may be wise to point out these
small errors when you thank the submitter for their patch, however, if you don't
want to see the same errors in future patches.

## Applying patches

In order to integrate the changes, you need to *apply* the patch. The tool for
this is [`hg import`][hg-import]. The difficult part here is going to be
obtaining a copy of the email to provide to `hg import`. Some clients like
[mutt][mutt] make this easy (in mutt, you can use the `|` key to pipe an email
directly to `hg import -`), or tools like [offlineimap][offlineimap] can help (or
a combination of the two!). Most popular end-user clients do not provide this
option. If you're in this boat, the easiest way to get a raw email is to use the
"raw" link on lists.sr.ht, which is hidden away under the "details" button.

[hg-import]: https://www.mercurial-scm.org/doc/hg.1.html#import
[mutt]: http://www.mutt.org
[offlineimap]: http://www.offlineimap.org/

If you copy the link to the raw email from lists.sr.ht, you can pass that
directly to `hg import` and it will download it for you:

    hg import https://lists.sr.ht/...

You can also just run `hg import -` paste the patch into it, followed by Ctrl+D.

If you use the [evolve](https://www.mercurial-scm.org/doc/evolution/)
extension, it is advised to also add the `--obsolete` option to `hg import`
command. This way, the contributor would see their changesets "evolved" when
pulling from the upstream repository.

Once applied, you can make these commits available upstream by using [`hg
push`][hg-push] normally. Don't forget to send the contributor a thank you
email!

[hg-push]: https://www.mercurial-scm.org/doc/hg.1.html#push
