---
title: hg.sr.ht Configuration
---

This document covers the configuration process for hg.sr.ht.

# Cronjobs

- `hgsrht-periodic`: The recommended configuration is
  `*/20 * * * * hgsrht-periodic`.
- `hgsrht-clonebundles` (optional): It is recommend to run this job daily.

# Storage

## Repository

<div class="alert alert-info">
  <strong>Note:</strong> If hg.sr.ht was installed in a package, you may skip
  this section.
</div>

As a repository hosting service, hg.sr.ht requires a place for storing
repositories (we recommend `/var/lib/mercurial/`). It also requires a
`hg` user who has ownership over the repository storage location.

# SSH Dispatch

At the moment, hg.sr.ht uses git.sr.ht's SSH dispatcher, which you need to set
up as the system-wide SSH authorization hook. See git.sr.ht's [Configuration
page](/git.sr.ht/configuration.md) for more information.

Once this is done, you need to make the hg.sr.ht authorization hook be part of
the SSH dispatching, via the `hgsrht-keys` and `hgsrht-shell` scripts. See the
`[dispatch]` section of your hg.sr.ht configuration for more details.

Authorization logs are written to `hg-srht-shell`.

# HTTP(S) Cloning

hg.sr.ht does not do HTTP(S) cloning for you, so you'll need to set it up
yourself in Nginx. Here's an example Nginx configuration:

```nginx
location = /authorize {
		proxy_pass http://127.0.0.1:5010;
		proxy_pass_request_body off;
		proxy_set_header Content-Length "";
		proxy_set_header X-Original-URI $request_uri;
}

location ~ ^/[~^][a-z0-9_]+/[a-zA-Z0-9_.-]+/\.hg.*$ {
		auth_request /authorize;
		root /var/lib/mercurial;
}
```

It is important that you set up the `/authorize` endpoint to enforce the
privacy of private repositories.
