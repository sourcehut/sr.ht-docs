---
title: hg.sr.ht docs
---

[hg.sr.ht](https://hg.sr.ht) is a service for hosting mercurial repositories on
sr.ht.

**See also**:

- [Installation guide](installation.md)

**Important**: hg.sr.ht is a work in progress. As a consequence, URLs of
specific files, commits, branches, etc in the repository viewer are **NOT**
guaranteed to remain consistent forever. When we finish writing our new
repository viewer, these will change.

# New to mercurial?

Learning how to use mercurial in general is out of the scope of our
documentation. Here are some good resources:

- [Mercurial: The Definitive Guide](https://book.mercurial-scm.org/read/)
- [Learning Mercurial in Workflows](https://www.mercurial-scm.org/guide)
- [Mercurial man page](https://www.mercurial-scm.org/doc/hg.1.html)

We do have some general resources for helping you use mercurial with sr.ht:

- [Using hg-email for sending and retrieving patches on sr.ht](email.md)

# SSH host keys

If you are cloning via ssh, these lines should be added to
`~/.ssh/known_hosts`.

```
hg.sr.ht ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3JDm3Ld1HNWuc+Z7KOgqboF5PVWqxRpTYHf0Lu3s4EVnCI8VlemCE7dcrfr5wffND/izSkxCf+RSWseT7oRY3Nz5VKVoAXE50Z5z+ASEeKQ5sevXCo5Rn7827Co4//J/iIWbRSbKpFR1GnhBzbNvbA5jBWP3oK6+1oqXIrgKS0i5tFk5pMBBGkOi1l6L9FxdMRe1pvx7ZkXXrSsb8uVdBD4bz9WiNKbL4qdDchL3vjMNOhdgBwBIQIg/hk4gyJfbIW/qNhZfSiwrFBekLI88tao72PycZebCSMR/YRX4OZr//5L4KNwLV81AhLQuiWRyEucw9uS/lv3gKBCaVOSoH
hg.sr.ht ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBL4aNGa+KnvMA0QoWrIVuI2QBU0Q/xX48sMBl3VtP/zPOGMvS50zGVMaA00RSzfcI2X0v/aUTsVm2vBNo/V1gTg=
hg.sr.ht ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAIcGGJcZHhcKBbsdaG+MwqZAarpJI+6nahwPaA1GK34
```

# Acceptable resource usage

The guidelines on resource usage are somewhat flexible. Here are some tips:

- As an approximation, aim to have repos no larger than the Linux kernel
  git repo, which is 3.5 GiB.
- Don't store lots of large binary objects in your repository, such as images,
  audio, or video. A couple of small binary files (such as your logo or a
  screenshot) is fine.

If in doubt, you're probably fine. We monitor resource usage and we'll send you
a friendly email if we notice that you're using too much.

# hg.sr.ht manual

The following sections document various features of hg.sr.ht.

## Repository creation

Creating new repositories is done on the [create page](https://hg.sr.ht/create),
which you can access from any page via the navigation on the top right. You can
also create a new repository by pushing to one that does not yet exist, and
clicking the link which is printed in the `hg push` output to complete the
creation process.

The *name* and *description* fields are used to describe your repository to the
public. The *visibility* field will change how your repository is shown on the
website. Public repositories are visible on your profile, to anonymous users,
and on third-party search engines. Unlisted repositories are visible to anyone
who has the link, but are not shown in your profile or in search results.
Private repositories are only visible to you and other logged-in users who you
explicitly [grant access to](#access).

## Settings

Each repository's settings may be accessed via the settings link on the
repository's detail page.

### Info

You can change the repository's visibility and description on this page. Click
"Rename?" if you wish to rename your repository. Renaming your repository sets
up a redirect from the previous name to the new.

### Access

The access page allows you to grant other users read or write access to your
repository. Users with read access will be able to clone and view private
repositories on the web, and users with write access will be able to push new
to your repository. Users with write access will not be able to edit your
repository's settings.

### Delete

You can delete your repository from this page. This operation cannot be undone.
This does not delete any other resources that may be associated with this
project, such as a ticket tracker on todo.sr.ht or build history on
builds.sr.ht.

## Profile

Your public profile is available at hg.sr.ht/~username. Here any public
repositories on your account are listed and searchable, but unlisted and private
repositories are hidden unless you're logged in.

The information displayed on your profile page is sourced from your [meta.sr.ht
profile](/meta.sr.ht#profile).

