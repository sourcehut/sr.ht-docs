---
title: hg.sr.ht Installation
---

This document covers the installation steps for
[hg.sr.ht](https://hg.sr.ht/~sircmpwn/hg.sr.ht), the hg repository hosting
service.

# Installation

hg.sr.ht can be installed with the [standard package
installation process](/installation.md#installing-from-packages).

## Daemons

- `hg.sr.ht` — The web service.

## Cronjobs

- `hgsrht-periodic`: Performs various maintenance tasks.
- `hgsrht-clonebundles` (optional): Generates [clone
  bundles](https://www.mercurial-scm.org/wiki/ClonebundlesExtension).

## Configuration

See [Configuration](configuration.md).
