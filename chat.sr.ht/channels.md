# Starting an IRC channel for your project

For information about how to register an IRC channel on the Libera Chat network,
consult the [Libera Chat documentation on channel registration][0].

[0]: https://libera.chat/guides/creatingchannels

Once you've set up your channel, you can add a link to it in your project README
like so:

    [Join us on IRC](irc://irc.libera.chat/#channelname)
