# Using the chat.sr.ht bouncer

For full details, refer to the [soju(1)] man page. Information about common
use-cases is provided here.

[soju(1)]: https://soju.im/doc/soju.1.html#IRC_SERVICE

## Connecting with a third-party IRC client

Check out the [quickstart for experienced IRC users][quickstart] for details on
configuring your own IRC client for use with chat.sr.ht.

[quickstart]: /chat.sr.ht/quickstart.md

## Managing your IRC networks

The webchat at [chat.sr.ht] provides a UI for managing your bouncer networks. On
the chat.sr.ht tab, you can add new network using the `Add network` button on
the top-right of the screen.

When a new network is added, a tab is created for it and there you can join a
channel using the `Join channel` button or manage the network settings using the
`Manage network` button.

Third-party clients with soju support may detect the bouncer and offer similar
functionality. If not, you can also message BouncerServ to configure soju. Use
`/msg BouncerServ help` for a full list of supported commands.

To add a new IRC network via BouncerServ:

```
/msg BouncerServ network create -addr <hostname> [options...]
```

See [soju(1)] for details on the supported options.

[chat.sr.ht]: https://chat.sr.ht

## Authentication

In IRC, communicating with a user requires knowing their "nickname" ("nick" for
short).  Traditionally, IRC would allow users to pick any nick they like -- even
one commonly used by someone else.  Today, in order to "register" a nick so that
others cannot use it, a system called SASL is used. See
[Nickname Registration][libera.chat-registration] for details on how to register
a nickname on Libera Chat.

[libera.chat-registration]: https://libera.chat/guides/registration

chat.sr.ht uses SASL to automatically log you in to the networks you use, and
prompts you for your login details when you first connect to a new network.

If you didn't provide your SASL credentials when connecting to the network, you
can do it afterwards by going into the network tab you want to authenticate and
click the `authenticate` link.

Additionally, SASL can be manually configured for soju(1) using BouncerServ;
see the `sasl` command (try `/msg BouncerServ help sasl set-plain`).

Note that IRC networks are independent with regards to nickname registration;
you must register your nickname manually for every IRC network you use.  Beware
that your nickname may be registered by someone else on a network already, and
that nickname registrations may be inconsistent across the networks you use (a
nickname may be registered on one network but available on another).

Unfortunately, not all networks support SASL.  There are alternative methods for
nickname management, which are not recommended but in such cases necessary.  The
effectively-universal but now-legacy system is NickServ, an IRC bot which can be
messaged to control it (to try using it, send it the message "help").  Note that
there is an independent instance of NickServ on every network.  To log in using
NickServ, you have to message it an "IDENTIFY" command every time you connect to
a network.  This can be automated using advanced configuration within soju,
BouncerServ provides the `-connect-command` network option for this:

    /msg BouncerServ net update <network> -connect-command "PRIVMSG NickServ :IDENTIFY <password>"

CertFP is another commonly-available system, but can be difficult to set up for
the first time -- see the [libera.chat guide][libera.chat-certfp], and then the
`certfp` command within BouncerServ.

[libera.chat-certfp]: https://libera.chat/guides/certfp

> [The OFTC network](https://oftc.net) is relatively popular, and currently does
> not provide SASL support.  This is why chat.sr.ht does not prompt the user for
> SASL-based login details when connecting to it.

## Detaching from channels

You can instruct the bouncer to "detach" from a channel. This causes it to be
hidden from your channel list, but to leave your connection to the channel
active. If you're mentioned by a user in this channel, you will receive
notifications for it, and if you re-join later, you will be able to view logs of
discussions which took place in your absence.

To detach from a channel, use the `/part detach` command, or `/msg BouncerServ
channel update <name> -detached=<true|false>`.

## Client-specific configuration

Various configuration recommendations for third-party IRC clients are documented
here: [contrib/clients.md](https://git.sr.ht/~emersion/soju/tree/master/item/contrib/clients.md).

## Chat history logs

chat.sr.ht saves logs for all your discussions. Chat history can be fetched via
the [IRCv3 chathistory extension](https://ircv3.net/specs/extensions/chathistory).
For instance, the following command fetches the latest 100 messages sent to the
\#sr.ht channel:

    /quote CHATHISTORY LATEST #sr.ht * 100

To mirror logs locally, [chathistorysync](https://git.sr.ht/~emersion/chathistorysync)
can be used.
