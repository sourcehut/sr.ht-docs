# chat.sr.ht quick-start for experienced IRC users

chat.sr.ht provides an IRC bouncer, which maintains a persistent IRC session for
you with one or more "upstream" IRC networks. For example, you may connect to
chat.sr.ht, then chat.sr.ht will connect to [Libera Chat][libera] for you, and
it will maintain a persistent connection while you're away. Setting up
chat.sr.ht is thus two steps: connecting to chat.sr.ht, and configuring which
upstream networks you want chat.sr.ht to connect to.

[libera]: https://libera.chat

The webchat is available at [https://chat.sr.ht](https://chat.sr.ht) and
requires no configuration to use. It will automatically connect you to your
bouncer account. Instructions for configuring your upstream networks are then
shown on-screen.

## Connecting with your own IRC client

You can also connect directly to the bouncer using your own IRC client. The
easiest way to use the bouncer is to use a [plugin for your client][config]
which implements the "soju.im/bouncer-networks" extension. This allows you to
set up your bouncer connection details just once, then automatically configure
all of your upstream IRC networks. If your client does not have a supported
plugin, or you would prefer to configure networks manually, see the next
section.

[config]: https://git.sr.ht/~emersion/soju/tree/master/item/contrib/clients.md

To set up your bouncer connection, you will need to generate an OAuth 2.0
personal access token to use as the server password. Visit [the personal access
token issuance page with this specially-crafted link][token] to generate a token
with the required scopes. Then add a network to your IRC client using the
following credentials:

[token]: https://meta.sr.ht/oauth2/personal-token?grants=meta.sr.ht/PROFILE:RO

<dl>
  <dt>Hostname</dt><dd>chat.sr.ht</dd>
  <dt>Port</dt><dd>6697</dd>
  <dt>SSL/TLS</dt><dd>Yes</dd>
  <dt>SASL</dt><dd>Yes</dd>
  <dt>Username</dt><dd><em>your sourcehut username</em></dd>
  <dt>Password</dt><dd><em>your personal access token</em></dd>
</dl>

This will connect you to the bouncer, but you will still have to configure any
upstream networks you wish to use by chatting with the special "BouncerServ"
user. Use `/msg BouncerServ help` for a full list of supported commands.

To add a new IRC network via BouncerServ:

```
/msg BouncerServ network create -addr <hostname> [options...]
```

See [soju(1)] for details on the supported options.

[soju(1)]: https://soju.im/doc/soju.1.html#IRC_SERVICE

You may have to restart your client to auto-connect to any new networks.

## Connecting without a client plugin

Connecting without a plugin is also possible. Using the same configuration
settings recommended above, connect at least once and configure your upstream
networks with BouncerServ (or set them up with the webchat). Then, configure a
new IRC network in your IRC client for each upstream network you want to connect
to using the same settings, but update your username to the format
"*username*/*network-name*". For instance, I use "sircmpwn/liberachat" to
connect to my Libera upstream.

## Multi-client identification

If you intend to use multiple clients, it is recommended that you append
"@*clientname*" to your username. For example, I might use
"sircmpwn@workstation" as my username for my desktop PC, and "sircmpwn@phone"
for my smartphone. If you are configuring networks manually instead of using a
plugin, the client ID is placed after the network name:
"sircmpwn/liberachat@workstation".
