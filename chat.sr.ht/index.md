# chat.sr.ht

chat.sr.ht provides real-time chat services to sourcehut users based on the
Internet Relay Chat ([IRC][0]) protocol. In technical terms, chat.sr.ht
essentially provides a hosted IRC bouncer.

**NOTICE**: chat.sr.ht is presently only available for users with a paid
sourcehut account. You can set up billing [on meta.sr.ht][billing].

[billing]: https://meta.sr.ht/billing

SourceHut does not operate an IRC network. We recommend [Libera Chat][1], an
independently operated, third-party network, which chat.sr.ht is configured to
use by default. Please review the Libera Chat [Network policies][2] for details
on appropriate network usage.

[0]: https://en.wikipedia.org/wiki/Internet_Relay_Chat
[1]: https://libera.chat
[2]: https://libera.chat/policies

## chat.sr.ht documentation

To start using chat.sr.ht, simply visit [chat.sr.ht](https://chat.sr.ht) in your
web browser and log in with your SourceHut credentials. Read the instructions
on-screen to get started.

We have a few additional guides for different audiences:

- [Introduction to IRC etiquette](/chat.sr.ht/etiquette.md)
- [Quick-start for experienced IRC users](/chat.sr.ht/quickstart.md)
- [Using the IRC bouncer](/chat.sr.ht/bouncer-usage.md)
- [Setting up an IRC channel for your project](/chat.sr.ht/channels.md)
- [Writing an IRC bot](/chat.sr.ht/bots.md)
- [Information for network operators](/chat.sr.ht/ircops.md)

## chat.sr.ht software

chat.sr.ht is based on the [soju][3] IRC bouncer and the [gamja][4] webchat
client. Our downstream patches for soju are kept in the "srht" branch of [our
fork][5].

[3]: https://soju.im/
[4]: https://sr.ht/~emersion/gamja/
[5]: https://git.sr.ht/~bitfehler/soju/tree/srht

## Official IRC channels and support

SourceHut operates a number of official IRC channels on libera.chat where you
can get support for sr.ht issues or socialize with other SourceHut users. You
can join them with the `/join <channel>` command, or, if you have configured the
chat.sr.ht irc:// handler, click the link.

- [#sr.ht](irc://irc.libera.chat/#sr.ht): on-topic sourcehut support and
  development channel, including for technical issues related to chat.sr.ht's
  webchat and bouncer service
- [#sr.ht.watercooler](irc://irc.libera.chat/#sr.ht.watercooler): off-topic
  channel for discussing sourcehut-adjacent topics with the sourcehut community
- [#sr.ht.ops](irc://irc.libera.chat/#sr.ht.ops): read-only technical operations
  &amp; monitoring channel
- [#libera](irc://irc.libera.chat/#libera): official Libera network support, for
  issues with channel management, NickServ account maintenance, and moderation
  and spam concerns. This channel is not officially affiliated with SourceHut.
