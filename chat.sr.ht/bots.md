# Setting up IRC bots

IRC is a standardized protocol. The best resource for learning about the IRC
protocol is [ircdocs.horse][0], which essentially tracks the IRC living
standard. Many libraries are available for various programming languages to make
IRC bots easier to write, finding one for your language is an exercise left to
the reader. In any case, writing an IRC client implementation from scratch is
very easy.

[0]: https://ircdocs.horse/

**Do not bring your IRC bot into a channel without permission from the channel
operators**.

You can find various IRC-related software projects implemented by sourcehut
users [on the project hub][1].

[1]: https://sr.ht/projects?search=%23irc&sort=recently-updated
