# IRC Etiquette

IRC is the oldest internet chat system, and as such, it has more time than any
other to develop its internal culture and nuances of etiquette. This may be
intimidating to new users, so we've set up this guide to help you avoid making a
potential faux pas.

Note that the official SourceHut IRC channels have a culture of tolerance for
users who may not be familiar with IRC's norms. Users on our channels are
encouraged to be patient with new IRC users, so don't worry too much about
making mistakes. We will correct your errors, so that you won't make them
elsewhere, but we will be friendly about it.

## Basics of IRC

IRC is organized into *networks*, which are themselves made of *servers* and
have a discrete universe of *users* and *channels*. [Libera Chat][0] is a
popular IRC network. When you connect to a network, you can "join" channels to
participate in them with other users by using the "/join #*channel*" command.
You can also speak directly to other users with "/msg *user* hi *user*, ...",
but note that it is considered impolite to message a stranger directly without
getting their permission first.

[0]: https://libera.chat

IRC is a plain-text system. There is little to no inline styles, message quoting
or threading, inline multimedia, or long messages. And we like it this way! Try
to embrace these constraints as you participate on IRC: little workarounds like
`[inline markdown](https://example.org)` are considered poor taste.

IRC can be a social place, but more often it is a tool being used to get
something done. Most IRC channels are *on-topic*, which means that they are
based around a specific topic and discourage discussion on other topics. You can
find the topic for a channel with the "/topic" command, which will also often
include things like channel rules and other resources like documentation and
mailing lists. Read the topic!

## How to get answers

Many people go to IRC to get a question or questions answered, but a lot of
users go about this in an unproductive way. Here are some tips to get your
question answered.

First, **immediately state your question in detail**. Do not say, "hi, is there
anyone around?" or "can anyone help me with a JavaScript problem?" Instead, say
"hi, I have a problem with using Array.prototype.map in my JavaScript program."
Elaborate with as much detail as you can without waiting for someone to answer.
Be polite, but detailed and brief. Don't start by chatting about the weather if
what you ultimately want is to figure out a bug with some program.

Once you ask your question, **be prepared to wait**. Not everyone is paying
attention right away, or another discussion might be underway, or the right
person to answer your question is offline at the moment. You might end up
waiting anywhere from 30 minutes to several hours in some cases. If you end up
waiting longer than you wanted to, it might be wise to move to another medium
(such as an email to the appropriate mailing list). Don't repeat your question
over and over again: be patient. If it's been a while and the discussion has
moved on, you may politely re-state your question, but do this too much and you
will start annoying others.

While you're waiting, **do your research**. Keep working on the problem
yourself, keep digging through search engines and documentation and come back to
report progress as you discover more information. You should do your research
prior to asking as well: if you have a question which could easily be answered
by a quick reading of the documentation, you will be wasting everyone's time by
asking it. Also, if you find the solution to your problem in the course of your
own research, make sure to let the channel know the answer, too, so they can
stop looking into it themselves.

Finally, **remember the human**. Be polite and respectful to others, especially
to others from whom you expect help or advice. No one owes you anything, and
demanding it from them, no matter how badly you want it, is going to get you
banned from the chat. Remember to thank anyone who offers you their kindness and
help, and respect the instructions of the moderators (you can tell who they are
because they will typically have an "@" symbol next to their name). Presume good
faith, and don't mistake brevity for discourtesy: on IRC, brevity *is* a
courtesy.

## How to get someone's attention

You can get a specific person's attention by "highlighting" them (also known as
"mentioning" or "pinging" the person): simply use their name, and their client
will notify them. It is not necessary (or encouraged) to use an @ symbol. Note
that many clients will "tab complete" usernames if you start typing them and
press "tab".

It is encouraged to include enough details in the highlight for the user to
understand it out of context, if they see the notification later.

*Good*

```
<sircmpwn> minus: can you reboot the production server?
```

*Bad*

```
<sircmpwn> the production server needs to be rebooted
<sircmpwn> ping minus
```

Please do not highlight someone unless you need their attention *in particular*:
there are often many other people on the channel who can help you with a
problem, and the operators or project leaders are probably overwhelmed.

## Quoting other users

It is not usually necessary to quote someone to reply to them: simply state your
message and the natural flow of the conversation will generally make it obvious
that you're replying to someone. This is often true even if there are several
ongoing conversations at once. Often you can reply to someone without quoting by
highlighting them.

However, if the conversation has moved on, or you feel that it's necessary to
disambiguate your comment, you can quote someone by writing their comment as ">
*quoted text*", then, on a new line, adding your message.

*Example*

```
<minus> it's pretty rainy out today
[...5 minutes of unrelated conversation...]
<sircmpwn> >it's pretty rainy out today
<sircmpwn> minus: it's pretty sunny here in Amsterdam
```

## Don't paste lots of text into the chat

As a rule of thumb, do not post more than 3 lines of text at once into the chat.
Doing so is strongly frowned upon and is likely to get you banned from the
channel. If you have a lot of text to share, drop it on [paste.sr.ht] and share
the link instead.

[paste.sr.ht]: https://paste.sr.ht

## Various etiquette tips

Here's a few miscellaneous tips:

- Do not use the "SMS" writing style. It's spelled "you", not "u". IRC users
  generally have a full-sized keyboard at their disposal and are expected to use
  it, even if you happen to be on mobile.
- Don't use "away nicks": do not change your nickname when you are offline,
  instead use the /away command. chat.sr.ht will automatically set your away
  status when you close all of your active clients.
- Do not bring an IRC bot into someone else's channel without permission

Various other helpful resources on IRC culture:

- [My philosophy for productive instant messaging](https://drewdevault.com/2021/11/24/A-philosophy-for-instant-messaging.html)
- [Getting help on IRC](https://workaround.org/getting-help-on-irc/)
- [Ubuntu IRC guidelines](https://wiki.ubuntu.com/IRC/Guidelines)
- [tilde.chat IRC guidelines](https://tilde.wiki/wiki/IRC_etiquette_guide)
- [Julia Evans: How to ask good questions](https://jvns.ca/blog/good-questions/)
- [Libera.Chat guides](https://libera.chat/guides)
