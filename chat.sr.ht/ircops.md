# chat.sr.ht for IRC network operators

chat.sr.ht is a hosted IRC bouncer which provides service to paid sourcehut
users.

We use the `46.23.81.128/25` and `2a03:6000:1812:0100::/56` subnets for bouncer
connections, and we operate an identd.

Please subscribe to our [ircops mailing list][0] to receive important updates
regarding chat.sr.ht, such as planned changes to our IP address ranges. This
mailing list is only used for notifications which may require manual
intervention from third-party network operators, and will not receive marketing
emails or product updates.

[0]: https://lists.sr.ht/~sircmpwn/sr.ht-ircops

You can reach us for IRC-related questions at `contact@emersion.fr`.
