---
title: Package Repositories
---

The official SourceHut Alpine Linux package repositories are available at
[mirror.sr.ht](https://mirror.sr.ht).

<div class="alert alert-success">
  If you would like to contribute to the package repositories, we accept
  patches at <a href="https://lists.sr.ht/~sircmpwn/sr.ht-packages"
  class="alert-link">sr.ht-packages</a>.
</div>

<div class="alert alert-warning">
  <p>
    <strong>Warning:</strong> SourceHut is still in alpha, and has no stable
    releases. As such, we do not recommend packaging SourceHut for your upstream
    distribution repositories until we have shipped stable versions of our
    software.
  </p>
</div>

# Alpine Linux

**Mirror**: https://mirror.sr.ht/alpine/v3.20/sr.ht

**Signing key**: [alpine@sr.ht.pub](https://mirror.sr.ht/alpine/alpine%40sr.ht.rsa.pub)

**Maintainer**: Drew DeVault <sir@cmpwn.com>

**Source**: https://git.sr.ht/~sircmpwn/sr.ht-apkbuilds

```sh
# Add the following line to /etc/apk/repositories
https://mirror.sr.ht/alpine/v3.20/sr.ht

# Install signing key
wget -q -O /etc/apk/keys/alpine@sr.ht.rsa.pub https://mirror.sr.ht/alpine/alpine@sr.ht.rsa.pub

# Update package index
apk update
```

<div class="alert alert-warning">
  <strong>Warning:</strong> SourceHut shadows some upstream packages with
  patched versions. The SourceHut mirror must be the <em>first</em> repository
  in /etc/apk/repositories.
</div>

<div class="alert alert-warning">
  <strong>Warning:</strong> You cannot upgrade to new Alpine releases until we
  provide a repository for that Alpine version. Announcements of the build
  status of new Alpine releases are provided on the
  <a href="https://lists.sr.ht/~sircmpwn/sr.ht-admins" class="alert-link">sr.ht-admins</a> mailing list.
</div>

<div class="alert alert-warning">
  <strong>Warning:</strong> The Alpine Linux community package repository must
  be enabled.
</div>
