---
title: paste.sr.ht docs
---

[paste.sr.ht](https://paste.sr.ht) is an ad-hoc text file hosting service on
sr.ht.

# Getting Started

Pastes can be created by simply entering in the file contents on the front page
(when logged in). They can be optionally given a filename for easier
identification. Existing pastes created by you can be accessed via the
link on the sidebar.

To view the raw contents of a paste, use the "View raw" link at the top. This
allows for the paste to be downloaded using a tool like `wget` or `curl`.

**See also**

- [GraphQL reference](graphql.md)
- [Installation](installation.md)
