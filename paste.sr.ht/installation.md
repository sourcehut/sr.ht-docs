---
title: paste.sr.ht Installation
---

This document covers the installation steps for
[paste.sr.ht](https://git.sr.ht/~sircmpwn/paste.sr.ht), a pasting service.

# Installation

paste.sr.ht can be installed with the [standard package
installation process](/installation.md#installing-from-packages).

## Daemons

- `paste.sr.ht` — The web service.
