---
title: paste.sr.ht's GraphQL API
toc: false
---

paste.sr.ht's GraphQL API is compatible with the standard [SourceHut GraphQL API
conventions](/graphql.md).

See also:

- [paste.sr.ht GraphQL playground](https://paste.sr.ht/graphql)
- [paste.sr.ht GraphQL schema](https://git.sr.ht/~sircmpwn/paste.sr.ht/tree/master/item/api/graph/schema.graphqls)

