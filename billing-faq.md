---
title: Billing FAQ
---

[sr.ht-support]: mailto:~sircmpwn/sr.ht-support@lists.sr.ht

Sourcehut is a business, but it's also an open source project, made by people,
and made for people. If you have any questions or concerns about billing on
sourcehut, get in touch with us via the [sr.ht-support] mailing list.

# Who has to pay for an account?

You do not have to have a paid account to contribute to a project hosted on
sourcehut. You can participate in tickets and discussions, send patches, and so
on, without paying for your account. You can also do many of these things
without signing up for an account at all; many services provide links to
participate via email throughout the logged-out interface.

Maintainers who wish to host their own projects on sourcehut are expected to pay
for their account. To create repositories, mailing lists, trackers, or submit
build jobs, you will need to pay for your account.

**Notice**: sourcehut is currently considered at an alpha stage of development,
and the quality of the service may reflect that. However, the service is
reliable, stable, secure, and mostly complete at this stage of development. To
learn exactly what the alpha entails, consult [this document][alpha-details].
During the alpha, payment is encouraged, but optional, for most features.

[alpha-details]: https://sourcehut.org/alpha-details

# I don't think I can pay for it.

SourceHut does not price any users out of the service.

If the minimum fees are too high for your financial needs, or some other
circumstances prevent you from paying, then you can simply [send us an
email][sr.ht-support] explaining your situation, and you will be issued free
service. sr.ht users from many walks of life have been granted free service:
students, users with problems using their currency of choice, people between
jobs, and so on.

# Which payment methods do you accept?

Sourcehut accepts payments only with credit card (through
[Stripe](https://stripe.com) &mdash; we do not store or handle your card details
ourselves).

We currently do not accept any other payment methods, including cash,
cryptocurrency, bank wire, PayPal, etc.

If other circumstances prevent you from paying, [send us an
email][sr.ht-support] explaining your situation and we will try to work out a
solution.

# What are the differences between each plan?

There is no difference between any of the plans: they all have access to the
same level of service, and the same service limits. You should choose whichever
plan best represents your financial ability and your level of investment in
sr.ht. It's an honor system: pay what you think the service is worth to you.

# Why should I pay when GitHub, GitLab, etc, are free?

Hosting these services is not free, or even cheap. The money has to come from
somewhere.

Most other companies are financially supported by venture capital, in the form
of large investments from a small number of people. If you use their services
for free, or even if you pay the modest fees for their paid plans, they are not
incentivized to serve your needs first. They are beholden to their investors,
and at their behest, they may implement changes you don't like: invasive
tracking, selling your data, and so on.

SourceHut has not accepted, and will never accept, any outside investments. Our
sole source of revenue is from sr.ht users making their account payments. This
incentivizes us to only consider your needs, and not to use you as a resource to
be exploited. The financial relationship is much more responsible for both
parties.

Unlike GitHub and GitLab, your sr.ht subscription fee stays in open-source.
sr.ht is 100% free software, mostly licensed under the GNU Affero General Public
license (aka AGPL 3.0). We incorporate changes from the community and work
together to build the best service possible. Your payment provides for the cost
of operations, keeping someone online to maintain the site, developing new
features, and providing reliable service. We promise to only invest our profits
in free software or free culture efforts, and in fact we already sponsor many
such projects.

# Can I cancel or change my plan?

Yes, you can cancel or modify your plan at any time. Visit the [billing
page](https://meta.sr.ht/billing) to make changes. You will not be charged until
the service period you've already paid for expires.

# What will happen when I stop paying?

During the alpha, there are no consequences for non-payment.

After the alpha:

Your data will not disappear overnight.

The delinquency process is as follows:

- An email reminder will be sent to inform you that your account is past due
- After two weeks, your resources may become read only (e.g. you can `git clone`
  but not `git push`).
- After eight weeks, your case will be manually reviewed. We may choose to
  provide you with a complete export of your data and close your account. Note
  that this data export can be re-imported into sr.ht again should you choose to
  register for another account, or can be taken to a third-party instance, with
  little to no information loss.

# Will pricing change after the alpha?

Yes. The exact numbers have not been decided upon. There will be a generous
period of consultation with the community and advance warning before any pricing
changes take effect.

# I represent an organization and want to pay as a group.

The software implementation for user groups or organizations is prioritized for
the beta. In the meanwhile, utilize any workaround you wish: nominate one of
your members to host your resources on their account, or set up a dedicated
pseudo-account for the organization, or any other approach that suits your
needs. We will help you migrate to user groups once the feature is available.
[Shoot us an email][sr.ht-support] to let us know about your workaround
and we'll make a note on your account.

Billing considerations for organizations will be available when the beta begins,
but we don't know what they'll look like yet. The final discussion is deferred
pending the final software design.

# Can I get an invoice?

You can download invoices from the [billing page](https://meta.sr.ht/billing).

# Can I deploy sr.ht on my own infrastructure instead of paying for an account?

Yes. [Here are the instructions](https://man.sr.ht/installation.md).

# Do you offer paid support plans?

No, not at the moment.
