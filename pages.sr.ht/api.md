---
title: pages.sr.ht API reference
---

The pages.sr.ht API is a convenience wrapper around the [GraphQL API][0] and
allows one to easily publish a site with tools like `curl`.

# API endpoints

The following endpoints are available to users with an OAuth2 token valid for
the specified scope.

## POST /publish/:domain

**Scopes**: `PAGES:RW`

Publish a site.

The request body must use the `multipart/form-data` content type. The following
fields are supported:

- `content` (required): a .tar.gz file containing the new site content.
- `protocol`: select which protocol variant of the site to update. If unset,
  defaults to HTTPS.

A sub-directory can be appended to the URL (e.g. `/publish/example.org/subdir`)
in which case only the specified sub-directory is updated. The rest of the
files are left unchanged.

The response contains the new site version as plain-text.

[0]: https://srht.site/graphql
