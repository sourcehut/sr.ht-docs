---
title: pages.sr.ht Installation
---

This document covers the installation steps for
[pages.sr.ht](https://git.sr.ht/~sircmpwn/pages.sr.ht), a static website
hosting service.

This documentation is a work-in-progress.

# Installation

pages.sr.ht can be installed through [package
installation](/installation.md#installing-from-packages).

## Daemons

- `pages.sr.ht` — The web and API service.

## Configuration

See [Configuration](configuration.md).
